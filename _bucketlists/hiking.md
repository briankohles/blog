---
collection: bucketlists
layout: page
title: Hiking
author: Brian Kohles
tags: bucketlist hiking
categories: bucketlist
---

- **Arizona**
  - [ ] AZ Trail
  - [ ] [The Wave (Coyote Buttes North)](https://www.blm.gov/programs/recreation/permits-and-passes/lotteries-and-permit-systems/arizona/coyote-buttes-north) - Permit needed
  - [ ] Coyote Buttes South
  - [ ] [Aravaipa Canyon Wilderness](https://www.blm.gov/visit/aravaipa-canyon-wilderness) - Permit needed
  - **Sedona Area**
    - [ ] Lookout Mountain trail
    - [ ] Piestewa Peak
    - [ ] Infinite Loop
    - [ ] Sterling Pass to Vultee Arch
    - [ ] Vortex near airport
    - [ ] West fork of oak creek canyon
    - [ ] Slide rock state park
  - **Chiracauwa National Monument**
    - [ ] Silver Peak
- **Wisconsin**
  - [ ] Peninsula State Park
    - [ ] Eagle Bluff Loop
    - [ ] Eagle Trail
  - [ ] Point Beach Trail
  - [ ] Maribel Caves Trail
  - [x] Devil's Lake via West Bluff Trail
  - [ ] Omaha Trail
  - [x] Elroy-Sparta State Trail
- **Illinois**
  - [x] Rock Island Trail: Peoria to Dunlap
  - [x] Northbridge / Constitution Trail
  - [x] Starved Rock State Park
  - [x] Matheissen State Park
- **Utah**
  - [ ] Arches National Park
    - [x] Balanced Rock Loop Trail
    - [x] Delicate Arch Viewpoint Trail
    - [ ] Delicate Arch Trail
    - [x] Park Avenue Trail
    - [x] Arches National Park Driving Tour
  - [ ] Canyonlands National Park
    - [ ] Grand View Point Trail
    - [ ] White Rim Overlook Trail
    - [ ] Murphy Point Trail
    - [ ] Mesa Arch Trail
    - [ ] Aztec Butte Trail
    - [ ] Upheaval Dome via Crater View Trail
    - [ ] Syncline Loop
    - [ ] Whale Rock Trail
    - [ ] False Kiva Trail
  - [ ] Moab
    - [ ] Long Canyon OHV Route
    - [ ] La Sal Mountain Scenic Drive
    - [ ] Colorado River Day Float
  - [ ] Dead Horse Point State Park
    - [ ] Dead Horse Rim Loop Trail
    - [ ] Intrepid Trail: Big Chief Loop
