---
collection: bucketlists
layout: page
title: 4x4 Drives
author: Brian Kohles
tags: bucketlist 4x4 offroad 
categories: bucketlist
---

## Sedona, Arizona

- [ ] Greasy Spoon aka Diamondback Gulch OHV Trail - in reverse
  - Leads to Outlaw loop trail
    -Leads to ruins (Honakai Ruins) @ red canyon overlook
- [x] Schnebly Hill (down)
- [ ] Schnebly Hill (up)

