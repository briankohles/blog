---
collection: bucketlists
layout: page
title: Motorcycle Rides
author: Brian Kohles
tags: bucketlist Motorcycle rides
categories: bucketlist
---

## California
- [ ] Giant Sequoias
  - http://www.advpulse.com/adv-rides/ride-california-giant-sequoias/
- [ ] Carrizo Plane National Monument
  - https://www.advpulse.com/adv-rides/adventure-motorcycle-ride-carrizo-plain/
  - https://www.blm.gov/documents/california/public-room/brochure/carrizo-plain-national-monument-recreation-map-and-guide

## South Dakota
- [ ] Needles Highway