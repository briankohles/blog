---
collection: bucketlists
layout: page
title: Kayak Trips
author: Brian Kohles
tags: bucketlist kayak
categories: bucketlist
---

### Arizona
  - [x] Lower Salt River
  - [x] Colorado River - Willow Beach to Emerald Cove
  - [x] Colorado River - Lee's Ferry to Glen Canyon Dam

### Utah
  - [ ] Antelope Island
  - [x] Colorado River  - Near Moab
    - 2016

### Wisconsin
  - [x] Apostle Islands Sea Caves
    - 2013
    - 2019
  - [ ] Devil's Lake

### Canada

  - [ ] Peterborough Lock Lift

### UK

  - [ ] Kayaking the Pontcysyllte Aqueduct, Llangollen Canal - Wales
