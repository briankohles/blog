# Camping/Biking Weekend Packing List

  NOTE: Items are checked as they are prepared & crossed through on paper as they are loaded

## Day before

- [x] wash truck

## Morning of

- [x] Oil Change for truck @ Big Two Toyota - 7:23am
- [ ] walk Sami
- [ ] put towels in washer
- [ ] fill camelback with water
- [ ] fill jug with water

## Clothing

- **Friday** - Wearing
  - [ ] Patagonia Shirt
  - [ ] Zip off pants
  - [ ] Underwear
  - [ ] Socks
  - [ ] driving shoes
  - **Sleeping**
    - in duffel
      - [x] long sleeve shirt
      - [x] sleep pants
      - [x] warm hat
      - [x] neck buffs to wear as a hat
- **Saturday** - Ride
  - In Duffel
    - [x] Bike Shorts
    - [x] Bike Socks
    - [x] Jersey
- **Saturday** - Drive Home
  - In Duffel
    - [x] t-shirt
    - [x] underwear
    - [x] socks
    - [ ] Shorts? or pants from friday?

## Camping

- **Electronics**
  - [x] drone
  - [x] 360 cam
  - [ ] iphone charger
  - [ ] Apple watch charger
  - [x] large battery pack (camelback)
  - [x] iphone bike mount
    - in bin by bike shoes
  - [x] small battery pack & long cable
  - [x] cable ties
  - [x] biking iphone
- [x] puffy jacket
- [x] walkie talkies
- [x] mask
- [x] pillow
- [x] blanket
  - warmth & changing if needed
- [x] tent
- [x] sleeping pad
- [x] sleeping bag
- [x] camp chair
- [ ] larger chair?
- [x] camp stove
  - [x] camp stove fuel
- [x] entertainment
  - [x] bocce balls
  - [x] playing cards
- [x] headphones
- [x] large garbage bags - for trash or wet/dirty gear
- [x] Toilet Kit
  - [x] Shovel
  - [x] toilet paper
  - [x] plastic bags
  - [x] hand sanitizer wipes
- [x] Toiletries - in duffel
  - [x] deodorant
  - [x] tooth brushes (4)
  - [x] assorted wipes
  - [x] shower wipes

## Food 

- [ ] cooler
  - [ ] soda x2
  - [ ] hard boiled eggs
  - [ ] cheese sticks
  - [ ] fruit
- [ ] meals
  - [ ] dinner
    - shepards pie
    - soda
  - [ ] breakfast
    - eggs
    - [x] coffee & tea
- [x] 3 gallon water container
- [x] Snacks
- [x] Dishes/Utinsels/Other
  - [x] Spork
  - [x] Cup

## Biking

- [x] Shoes (by hitch parts)
- [x] Camelback
  - [x] Find all day pack tools
  - [x] Gather tool set in camelback
  - [x] patch kit
  - [x] trail pump
  - [x] bike multi tool
  - [x] knife multitool
  - [x] duck tape
  - [x] Spare Tube
  - [x] snacks
  - [x] Headlamp & spare batteries
  - [x] First Aid Kit
  - [x] large batery pack
  - Clothes
    - [x] Jacket
    - [x] Gloves
      - warm & cool weather
    - [x] Helmet
  - [x] 360 selfie stick
- [x] Put slime in tires
- [x] Check shifter adjustment
- [x] Bike
- [x] Mount hitch
  - [x] pin & lock in yellow bin
- [x] Floor pump
- [x] Repair Stand
- [x] Bike toolbox

## On the way out of town

- [ ] Fill up truck with gas

## After Trip

- [ ] Buy Sunglasses
- [ ] Buy new baseball cap
- [ ] Put blue firstaid kit in truck
- [ ] look into selling all bikes (& versys) & getting a new FS bike (maybe ebike)
- [ ] need new mountain bike shoes
- [ ] New helmet (MIPS)
- [ ] New Mirrors for KLR
- [ ] matches
- [ ] lighter
- [ ] camp table
- [ ] bike shorts
- [ ] bike shoes

