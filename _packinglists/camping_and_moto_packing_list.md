# Camping/Moto Weekend Packing List

  NOTE: Items are checked as they are prepared & crossed through on paper as they are loaded

## To Buy Before

- [ ] Moto Tube Patch Kit #buy
- [ ] matches #buy
- [ ] lighter #buy
- [ ] camp table #buy
- [ ] Buy camp food #buy
  - [ ] Pouches #buy
  - [ ] Energy bars #buy
  - [ ] Snacks #buy
  - [ ] Beverages #buy
  - [ ] Water #buy
- [ ] 12V for moto #buy
- [ ] Larger Sleeping Pad for Car Camping #buy
- [ ] New Car, Could I sleep in Sherri's Car #buy
- [ ] Dash extender lifter thing #buy
- [ ] Buy new helmet #buy
- [ ] Buy goggles #buy
- [ ] Tank bag, something small for drone remote & such. #buy
- [ ] Thicker air mattress #buy
  - [ ] Bw says X-ped #buy
- [ ] Buy Sunglasses #buy
- [ ] Buy new baseball cap #buy

## Before

- [ ] Take Bike out for a long ride in the next few days to charge battery & wear down tires before changing
- [ ] wash car
- [ ] Put blue firstaid kit in car
- [ ] Determine Routes
- [ ] Send details to Sherri on where we are going
- [ ] Make sure moto tags & OHV are up to date
- [ ] Check Public Land permit
  - [ ] Make sure a copy is in moto jacket
- [ ] Mark Ratchet Straps as being mine
- [ ] Put on new tires
- [ ] Pack moto bags with everything they need
- [ ] Charge
  - [ ] Sena
  - [ ] Drone
  - [ ] Go Pro
  - [ ] 360
  - [ ] Garmin
- [ ] Wipe Memory Cards
- [ ] Load maps to Moto iPhone
    - [ ] Google maps
    - [ ] gaia GPS maps 
- [ ] Put iPhone Mount on Moto
  - [ ] rubber holder

## Day before

- [ ] Check Weather
- [ ] Get Cash
- [ ] Check Tire Pressure on Bike
- [ ] Deliver Motorcycle to Brian
  - [ ] Pack Moto Bags & load them on
  - [ ] Ratchet Straps--Qty: 4
  - [ ] Key
- [ ] Fill up Bike with Gas?
- [ ] Fill up car with gas
- [ ] Set Car GPS to have waypoints
- [ ] Put extra moto Key Moto Jacket (is there an interior Pocket)

## Morning of

- [ ] walk Sami
- [ ] Load Car
- [ ] Set garmin to do tracks every 30 minutes, turn off bluetooth, start recording
- [ ] Drive To Brian's

## Clothing

- **Friday** - Wearing
  - [ ] Shirt
  - [ ] Comfy Pants
  - [ ] Underwear
  - [ ] Socks
  - [ ] driving shoes
  - [ ] Wallet
    - [ ] National Parks Pass
    - [ ] Cash
  - [ ] Sun Glasses
  - [ ] Baseball Hat
- **Duffel**
  - **Sleeping**
    - [ ] long sleeve shirt
    - [ ] sleep pants
    - [ ] warm hat
    - [ ] Warm Socks
    - [ ] neck buffs to wear as a hat
  - **Saturday** - Ride
    - [ ] Underwear
    - [ ] Tech Tshirt
    - [ ] Running Shorts
    - [ ] Moto Socks
  - **Sunday** - Ride
    - [ ] Underwear
    - [ ] Tech Tshirt
    - [ ] Running Shorts
    - [ ] Moto Socks
  - **Sunday Drive Home**
    - [ ] t-shirt
    - [ ] underwear
    - [ ] socks
    - [ ] Shorts? or pants from friday?

## Camping

- **Electronics**
  - [ ] SD Card Case & cards
  - [ ] 360 cam
    - [ ] 360 selfie stick
  - [ ] go pro
    - [ ] USB charger back
  - [ ] drone
  - [ ] iphone charger
  - [ ] Apple watch charger
  - [ ] small battery pack & long cable
  - [ ] cable ties
  - [ ] biking iphone
  - [ ] walkie talkies
  - [ ] Ham Radio
    - [ ] Extra Battery
    - [ ] Charger
    - [ ] Long Antenna
- [ ] puffy jacket
- [ ] mask
- [ ] pillow
- [ ] blanket
  - warmth & changing if needed
- [ ] tent
- [ ] sleeping pad #buy
- [ ] sleeping bag
- [ ] camp chair
- [ ] Headlamp
  - [ ] Extra Batteries
- [ ] larger chair? #buy
- [ ] camp stove
  - [ ] camp stove fuel
  - [ ] Lighter #buy
- [ ] entertainment
  - [ ] bocce balls
  - [ ] playing cards
- [ ] headphones
- [ ] large garbage bags - for trash or wet/dirty gear
- [ ] Toilet Kit
  - [ ] Shovel
  - [ ] toilet paper
  - [ ] plastic bags
  - [ ] hand sanitizer wipes
- [ ] Toiletries - in duffel
  - [ ] deodorant
  - [ ] tooth brushes (4)
  - [ ] assorted wipes
  - [ ] shower wipes





## Food 


- **Friday**
  - Lunch: Stop on the way down, or eat before hand
  - Dinner: Camp Meal
- **Saturday**
  - Breakfast: Camp Meal
  - Lunch: Pack Something easy for on the road Lots of extra energy bars in camelback as reserve
  - Dinner: Camp Meal
- **Sunday**
  - Breakfast: Camp Meal & Coffee
  - Lunch: Pack Something easy for on the road Lots of extra energy bars in camelback as reserve
  - Dinner: Stop Somewhere on the way back
- [ ] cooler
  - [ ] soda #buy #Qty-4
  - [ ] Some good ciders #buy #Qty-2
  - [ ] hard boiled eggs #buy #Qty-6
  - [ ] cheese sticks #buy #Qty-4
  - [ ] fruit #buy
- [ ] 3 gallon water container #buy
- [ ] Snacks #buy
- [ ] Dishes/Utinsels/Other
  - [ ] Spork
  - [ ] Cup

## Moto

- [ ] **Riding Gear**
  - [ ] Helmet
    - [ ] Sena
  - [ ] Boots
    - [ ] Tall Socks #Qty-3
  - [ ] Multi-Clava #Qty-3
  - [ ] knee protection
  - [ ] Jacket
    - **Jacket Pocket**
      - [ ] Ear Plugs
        - [ ] Foam Green
        - [ ] PinLock
      - [ ] Disposable Mask
      - [ ] Money
      - [ ] Insurance
      - [ ] State Land Permit
    - [ ] Protection Layer
    - [ ] Rain Layer
    - [ ] Insulation Layer
  - [ ] pants
    - [ ] Protection Layer
    - [ ] Rain Layer
    - [ ] Insulation Layer
  - [ ] Gloves Warm
  - [ ] Gloves Cool
  - [ ] Multiclava #Qty-3
- [ ] **Camelback**
  - [ ] Garmin
  - [ ] Large Battery Pack
  - [ ] MultiTool
  - [ ] snacks
- [ ] **Panniers**
  - [ ] patch kit
  - [ ] Electric Pump
  - [ ] duck tape
  - [ ] Tow Rope
  - [ ] Spare Tubes
  - [ ] First Aid Kit
  - [ ] large battery pack
  - [ ] Waterproof bags
  - [ ] bungies

## On the way out of town

- [ ] Meet Brian @ his house

## After Trip

- [ ] look into selling all bikes (& versys) & getting a new FS bike (maybe ebike)
- [ ] need new mountain bike shoes
- [ ] New helmet (MIPS)
- [ ] bike shorts
- [ ] bike shoes

