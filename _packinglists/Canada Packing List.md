Packing List - Canada:
	Clothing:
		- Rain Jacket @done
		- Baseball Cap @done
		- Sneakers @done
		- Sandals @done
		- Bandana @done
	Outdoors:
		- Camelbak @done
	Hiking:
		- Hiking Boots @done
	International Travel:
		- Passport - Book @done
		- Passport - Card @done
		- Cell Phone Roaming Plan @done
	EDC:
		- Cash @done
		- Sunglasses @done
		- Knife - Dime @done
		- iPhone @done
			- iPhone Charger @done
		- Battery Pack @done
	Toiletries:
		- Toothbrush @done
		- Toothpaste @done
		- Deodorant @done
		- Allergy Medicine @done
		- Electric Razor @done
		- Disposable Razor @done
	Photography:
		- GoPro @done
		- GoPro Batteries @done
		- GoPro Mounts @done
		- SLR Camera @done
		- SLR Lenses @done
		- SLR Batteries @done
	Road Trip:
		- Car Mount for Phone @done
		- Car Charger for Phones @done
	Kayaking:
		- Rash Guard Shirt @done
		- Swim Trunks @done
		- Water Shoes @done
		- Towel - after @done
		- Change of Cloths - after @done
		- Wide Brimmed Hat @done
		- Sunscreen @done
		- Lip Balm @done
		- 
