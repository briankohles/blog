---
layout: page
title: Recipes
---

These are my recipes, and recipes that I use, credit provided when known.

The plan is to have all my recipes in markdown format using the guide provided by the team at the [Grocery-Smart Shopping List App](https://apps.apple.com/us/app/grocery-smart-sorting-grocery-list/id1195676848).

The format is documented on their github: [Grocery-Recipe-Format](https://github.com/cnstoll/Grocery-Recipe-Format).

Super high level the format is:
<!-- Enable a code snippet that is copyable -->
{% capture code %}# Title
## Header
- Ingredient
Step
> Note{% endcapture %}
{% include code.html code=code lang="shell" %}

I will have a script or some kind of automation that will pull the recipes from gitlab & put them on iCloud drive for Grocery to access.

{% assign sorted_recipes = site.recipes | sort_natural: "title" %}
{% for list in sorted_recipes %}
  * [ {{ list.title }} ]({{ list.url }})
{% endfor %}
