---
title: Oatmeal, Craisin, White Chocolate, Macadamia Nut Cookies
author: Brian Kohles
tags: cookie recipe oatmeal craisin white chocolate
categories: recipe
layout: recipe
---

# Oatmeal, Craisin, White Chocolate, Macadamia Nut Cookies

## Wet Ingredients

Cream together:
- Butter | 1/2 Cup | Softened
- Shortening | 1/2 Cup | Butter Flavored
- Light Brown Sugar | 220 G | 
- Sugar | 100 G | 

Add in the other wet ingredients:
- Eggs | 2 |
- Vanilla Extract | 1 t |


## Dry Ingredients

Add in all the dry ingredients & slowly mix together:
- Flour | 1 1/2 Cups | 
- Baking Soda | 1 tsp |
- Cinnamon | 1 tsp | 
- Clove | 1 1/2 tsp | 
- Salt | 1/2 tsp | 

## Stir in the oats & other ingredients

- Oats | 3 Cups |
- Craisins | 1 Cup | 
- White Chocolate Chips | 1 Cup | 
- Macadamia Nuts | 1 Cup | 


## Bake

Preheat over to 350º F (325º F fan assist) for 11 minutes [Baking Cookies].

> Cooking time may be 10-16 minutes depending on cookie size