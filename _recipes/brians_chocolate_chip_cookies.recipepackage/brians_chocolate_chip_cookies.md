---
title: Brian's Chocolate Chip Cookies
author: Brian Kohles
tags: cookie recipe chocolate chip
categories: recipe
layout: recipe
---

# Brian's Chocolate Chip Cookies

> The latest version of my continually changing chocolate chip cookies

# Preheat Oven

- Preheat oven to 350°F (325°F fan assist)

## Wet Ingredients

Cream together until smooth:
- Butter | 1/2 Cup | Softened
- Shortening | 1/2 Cup | Butter Flavored
- Dark Brown Sugar | 225 g | 
- Sugar | 200 g | 

Add in the other wet ingredients:
- Eggs | 2 Whole |
- Vanilla Extract | 8 g |

## Dry Ingredients

Add in all the dry ingredients & slowly mix together:
- Flour | 333 g | 
- Baking Soda | 5 g |
- Baking Powder | 5 g |

## Add mix ins

Mix in Chocolate Chips and anything else you like:
- Dark Chocolate Bar | 2 C | Cut into chunks

## Bake

- Cover baking sheet in parchment paper
- Use a large cookie scoop and place on parchment
- Bake at 350º F (325º F fan assist) for 11 minutes [Bake Cookies].
- Remove from cookie sheet immediately and cool on parchment

> Cooking time may be 10-16 minutes depending on cookie size

## Other optional mix ins

> Some other Mix ins to try

- Heath Toffee Bits | 1 C | with or without chocolate
- Instant Espresso Powder | 1 T |
- Cinnamon | 1 T |
- Cayenne Pepper | 1/8 t |


