#!/usr/bin/perl

# copy all of our recipes from the _recipes Directory to icloud drive 
# rename the .md files to .recipes

my $baseDir = '/Volumes/Data_2TB/Users/briankohles/Documents/git/briankohles.gitlab.io/';
my $blogDir = $baseDir.'_recipes';

my $groceryDir = '/Volumes/Data_2TB/Users/briankohles/Library/Mobile Documents/iCloud~com~conradstoll~Grocery/Documents/Recipes';

# empty out the groceryDir to make sure there are no recipes already there
my $cleanDirCmd = "rm -rf '$groceryDir/*'";
print "CLEANDIRCMD:$cleanDirCmd;\n";
`$cleanDirCmd`;


# copy all recipes from blogDir to groceryDir
my $copyCmd = "cp -r $blogDir/* '$groceryDir/'";
print "COPYCMD:$copyCmd;\n";
`$copyCmd`;

# remove the yamlFrontMatter from all of the recipe files
