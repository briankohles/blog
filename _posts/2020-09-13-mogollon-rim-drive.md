---
title: Driving Mogollon Rim Road
date: 2020-09-13
author: Brian Kohles
tags: adventure rimroad fr300 offroad
categories: adventure
layout: post
---

## The Basics

Today was our first drive along the Mogollon Rim, it was a very beautiful drive, that took a total of about 10 hours round trip.

The rim road (FR 300) was a beautiful route & a nice road, you could do the whole route in pretty much any car if you had the road to yourself. But in places the road is narrow & you may have to move a tire off the edge to let others pass, so I'd recommend having some ground clearance so you don't scrape.

Our route is shown below starting from 87 & 260 in Payson, AZ, and ending at the same point.

<iframe src='https://www.gaiagps.com/datasummary/folder/95fbb11b-7b08-4915-9ba4-b490e791efa4/?layer=mapquestosm&embed=True' style='border:none; overflow-y: hidden; background-color:white; min-width: 320px; max-width:420px; width:100%; height: 420px;' scrolling='no' seamless='seamless'></iframe>

## The Hike
Before the drive we stopped @ Water Wheel Falls, for a short hike, we weren't really prepared for any type of hike, so we turned around at the water crossing, but the part we did (about 2/3 mile total) was good, with nice scenery.

<iframe class="alltrails" src="https://www.alltrails.com/widget/trail/us/arizona/water-wheel-falls?scrollZoom=false&u=i" width="100%" height="400" frameborder="0" scrolling="no" marginheight="0" marginwidth="0" title="AllTrails: Trail Guides and Maps for Hiking, Camping, and Running"></iframe>
<br />
<img src="https://www.alltrails.com/qrcode/trail/us/arizona/water-wheel-falls" />

## The Rim Road


The Rim Road was a beautiful drive. We only made a few stops this trip, since we knew we wanted to finish the whole thing & we had to get back home to the dog. There was one quick stop for lunch & a short off shoot to get to Myrtle Point. This road was more challenging. The last section has some large rocks, and some tight squeezes between trees. The Tacoma came out unharmed, but I was definitely concerned about getting some AZ pin striping.

Click into the Gaia GPS map above for details of our route & way points, or follow below for the info about the Rim Road from All Trails.

<iframe class="alltrails" src="https://www.alltrails.com/widget/trail/us/arizona/mogollon-rim-road-scenic-drive?scrollZoom=false&u=i" width="100%" height="400" frameborder="0" scrolling="no" marginheight="0" marginwidth="0" title="AllTrails: Trail Guides and Maps for Hiking, Camping, and Running"></iframe>
