---
title: Moment 58mm Telephoto Lens
date: 2020-11-08
author: Brian Kohles
tags: iphone moment lens
categories: photography
layout: post
---

## Moment 58mm Lens

The moment lens is a high quality add on lens for your iPhone. On the iPhone 11 Pro Max with it's 2x optical zoom this lens adds an additional 2x zoom, bringing optical zoom to a total of 4x optical. Sample pictures to show the zoom level are below.

You can find the lens on the [Moment Website](https://www.shopmoment.com/products/tele-58-mm-lens), it does require one of their cases or lens mounts as well.

> iPhone 11 Pro Max 0.5x wide angle

{% include image name="moment_05x.JPG" caption="0.5x" %}

> iPhone 11 Pro Max 1.0x wide angle

{% include image name="moment_10x.JPG" caption="1.0x" %}

> iPhone 11 Pro Max 2.0x wide angle

{% include image name="moment_20x.JPG" caption="2.0x" %}

> iPhone 11 Pro Max 2.0x wide angle with 58mm (2x) Moment Telephoto lens

{% include image name="moment_40x.JPG" caption="4.0x" %}

> Comparison of the zoom angle in a single image

{% include image name="moment_compare.jpg" caption="compare" %}