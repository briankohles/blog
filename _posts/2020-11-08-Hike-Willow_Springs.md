---
title: Hike - Willow Spring Trail
date: 2020-11-08
author: Brian Kohles
tags: hike willow_springs
categories: hike
layout: post
---

## The Basics

This hike was in a new area that I had not explored before. The trailhead is just north and across the street from the Weavers Needle Vista Viewpoint on AZ-88, starting @ the Bulldog OHV parking lot on AZ-88 & FR-12.

There are lots of trails in the area to choose from, we had planned a route of about 8 miles, but ended up cutting it very short due to time, as we spent a lot of time playing with the drone.

{% include image name="WillowSpring.jpg" caption="Willow Spring" %}

<a data-flickr-embed="true" href="https://www.flickr.com/photos/briankohles/50612789602/in/album-72157716936981057/" title="DJI_0001"><img src="https://live.staticflickr.com/65535/50612789602_68db723726_c.jpg" width="800" height="450" alt="DJI_0001"></a><script async src="//embedr.flickr.com/assets/client-code.js" charset="utf-8"></script>

## Map

<iframe src='https://www.gaiagps.com/public/XNb61EV9S0x0mfLCPxaAgxGT?embed=True' style='border:none; overflow-y: hidden; background-color:white; min-width: 480px; max-width:800px; width:100%; height: 420px;' scrolling='no' seamless='seamless'></iframe>

## Videos

While hiking we stopped @ a few spots to do some shots with my new Mavic Mini. I didn't like any of the shots that I did, so I'm sharing the ones that Jeremy shot.

<a data-flickr-embed="true" href="https://www.flickr.com/photos/briankohles/50612768522/in/album-72157716936981057/" title="Jeremys 1st Pan"><img src="https://live.staticflickr.com/31337/50612768522_c2793203bd_c.jpg" width="800" height="450" alt="Jeremys 1st Pan"></a><script async src="//embedr.flickr.com/assets/client-code.js" charset="utf-8"></script>

<a data-flickr-embed="true" href="https://www.flickr.com/photos/briankohles/50612651026/in/album-72157716936981057/" title="Jeremys 2nd Pan"><img src="https://live.staticflickr.com/31337/50612651026_8c23d6fcfa_c.jpg" width="800" height="450" alt="Jeremys 2nd Pan"></a><script async src="//embedr.flickr.com/assets/client-code.js" charset="utf-8"></script>



