---
title: Converting Paprika 3 Recipes to Markdown
date: 2020-06-30
author: Brian Kohles
tags: recipes code
categories: code
layout: post
---

## Update: 

I've completed this process and posted a new blog entry about it: [Read the latest update on this process]({% post_url 2021-08-07-Converting-recipes-paprika-to-markdown-conclusion %})

I've been working on converting all of my text based data to markdown to make it more portable.

I've found a great iOS app to use for my recipes, using [Grocery App]({% post_url 2020-06-29-Grocery_-_Smart_Shopping_List %}), which I covered yesterday.

Now the problem is that I need to get all my recipes out of Paprika 3 and into markdown. This post will cover that process as it unfolds, and provide the code needed if you come across the same problem.

## The general plan

At this point I've only done a quick review of what options Paprika provides for an export:
* HTML
* Paprika Format

The HTML code generated wasn't bad & I considered using pandoc, but I need to do a little additional formatting to get the data into the format Grocery App wants. And I like the format Grocery is using so I'm going all in with that.

So option two was Paprika Format, it turns out it isn't too bad & should be easy to script.

What I know so far:
* In Paprika 3 choose File -> Export...
* Provide a name & location
* Categories: All Recipes
* Format: Paprika Recipe Format
* Click Export

* This will provide a single file `My Recipes.paprikarecipes` (plural)
* CD to where this file was extracted
* running the linux/macOS command `file My\ Recipes.paprikarecipes` on this tells us it is a Zip archive.
* So we just need to run a simple `unzip My\ Recipes.paprikarecipes` to extract the file.
* remove this file so it doesn't cause future problems `rm 'My Recipes.paprikarecipes'`
* We now have a whole bunch of `*.paprikarecipe` (singular) files, these are oddly gzip files
* run gunzip to extract these `gunzip *.paprikarecipe`
* make sure rename is installed: `brew install rename`
* rename files to .gz from .paprikarecipe `rename s/\.paprikarecipe/\.gz/g *.paprikarecipe`
* extract all the recipes `gunzip *.gz`
* the files are now json format so rename them with the right extension `rename s/$/\.json/g *`
