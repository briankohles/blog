---
title: Hike - Willow Spring Trail
date: 2020-11-22
author: Brian Kohles
tags: hike willow_springs
categories: hike
layout: post
---

## The Basics

I wanted to get back out & check out more of the Willow Spring area. I followed a path that goes out to the right and around the closest peak. It was a much less travelled path, but worth the effort. I also tested out a bit of the hike out to the lake, and that was a good trip, except it was all wash, so I needed some gaiters to keep the rocks out of my boots. Those should be here later this week, and maybe I'll give that hike a better look, but it's about 15 miles out & back.

## Map

<iframe src='https://www.gaiagps.com/public/HlDPmoV838cKZmcc5mAXLDeC?embed=True' style='border:none; overflow-y: hidden; background-color:white; min-width: 320px; max-width:800px; width:100%; height: 420px;' scrolling='no' seamless='seamless'></iframe>

## Videos

<a data-flickr-embed="true" href="https://www.flickr.com/photos/briankohles/50639728163/in/dateposted-public/" title="Trim3"><img src="https://live.staticflickr.com/31337/50639728163_36ff0a54a5_c.jpg" width="800" height="450" alt="Trim3"></a><script async src="//embedr.flickr.com/assets/client-code.js" charset="utf-8"></script>

<a data-flickr-embed="true" href="https://www.flickr.com/photos/briankohles/50640479266/in/dateposted-public/" title="Trim2"><img src="https://live.staticflickr.com/31337/50640479266_22a9a98281_c.jpg" width="800" height="450" alt="Trim2"></a><script async src="//embedr.flickr.com/assets/client-code.js" charset="utf-8"></script>

<a data-flickr-embed="true" href="https://www.flickr.com/photos/briankohles/50639733503/in/dateposted-public/" title="Trim1"><img src="https://live.staticflickr.com/31337/50639733503_e6bff08f69_c.jpg" width="800" height="450" alt="Trim1"></a><script async src="//embedr.flickr.com/assets/client-code.js" charset="utf-8"></script>

