---
title: Grocery - Smart Shopping List
date: 2020-06-29
author: Brian Kohles
tags: code recipes
categories: code
layout: post
---

Several years ago after migrating my data from service, to srevice, to service I decided I'd had enough of proprietary formats & was going to move all of my data to non proprietary format. For most all my stuff that has worked out to be markdown.

I had not yet found a good way to store recipes in a nice structure . I'd searched a few times over the years & hadn't found what I wanted, then yesterday I happened across a github repo with some markdown recipe examples from the team that created [Grocery App](https://twitter.com/smartgroceryapp) for [iOS, iPadOS, watchOS](https://apps.apple.com/us/app/grocery-smart-shopping-list/id1195676848).

### The Format

Their format is dead simple, and super flexible, and best of all lets me put my recipes in the flow that I like of "do this steps with these things".

You can see on their Github page for [Grocery Recipe Format](https://github.com/cnstoll/Grocery-Recipe-Format) some example recipes, and the basic structure of a recipe:

<!-- Enable a code snippet that is copyable -->
{% capture code %}# Title
## Header
- Ingredient
Step
> Note{% endcapture %}
{% include code.html code=code lang="shell" %}

This simple format makes it very simple to throw a recipe together like the below. It's markdown so it's easy to read without any special tools, and the thing that I really like is that it is portable so I'm not locked into a specific recipe database format.

When I started this adventure I was not looking for a new shopping list app, nor was I looking for a new recipe storage app. But it happens that this little app does both of these things very well, and since it reads markdown, and easily syncs my data from iCloud I can easily workout a workflow to keep the recipes in the app updated with my blog store.

### The Shopping List

I'll admit I have not actually used this app yet for shopping, but from the little bit of playing around I've done it looks like a really nice app for that.

### The Recipe View

This is why I really liked this app, the recipe view is very simple and adds a lot of features from such a simple markdown format.

First it of course provides nice little check boxes so you can mark off each step and ingredient as you do it, but it also has a nice little marker you can use to note a timer. So just tapping the highlighted time, will start a timer, and then track multiple timers across multiple recipes.

### There was only one small problem
The app doesn't currently support YAML front matter. Front matter is the stuff that goes at the top of a lot of file types used for a lot of things, that defines some additional meta data about your content. In my case it is use by Jekyll that runs my blog/wiki/recipe box. The front matter in the code below is the part between the lines of `---` of my YAML/Markdown files or `+++` for TOML files. I've sent the Grocery team a feature request to see if they can ignore this section of the file, so hopefully that will come in a future update.

### Example Recipe

An example of the issue is below. For the below recipe if the front matter is included the Grocery recipe view looks like:
{% include image name="recipe_with_frontmatter.png" %}

Removing the front matter & everything looks perfect:
{% include image name="recipe_no_frontmatter.png" %}

<!-- Enable a code snippet that is copyable -->
{% capture code %}---
title: Brian's Chocolate Chip Cookies
author: Brian Kohles
tags: cookie recipe chocolate chip
categories: recipe
layout: recipe
---

# Brian's Chocolate Chip Cookies

> The latest version of my continually changing chocolate chip cookies

## Wet Ingredients

Cream together:
- Butter | 1/2 Cup | Softened
- Shortening | 1/2 Cup | Butter Flavored
- Dark Brown Sugar | 220 G | 
- Sugar | 200 G | 

Add in the other wet ingredients:
- Eggs | 2 |
- Vanilla Extract | 1 1/2 t |

## Dry Ingredients

Add in all the dry ingredients & slowly mix together:
- Flour | 333 G | 
- Baking Soda | 1 tsp |
- Baking Powder | 1 tsp |

## Add mix ins

Mix in Chocolate Chips and anything else you like:
- Dark Chocolate Bar | 2 C | Cut into chunks

## Bake

Preheat over to 350º F (325º F fan assist) for 11 minutes [Baking Cookies].

> Cooking time may be 10-16 minutes depending on cookie size

## Other optional mix ins

> Some other Mix ins to try

- Heath Toffee Bits | 1 C | with or without chocolate
- Instant Espresso Powder | 1 T |
- Cinnamon | 1 T |
- Cayenne Pepper | 1/8 t |{% endcapture %}
{% include code.html code=code lang="shell" %}