---
title: Converting Paprika 3 Recipes to Markdown - The Conclusion
date: 2021-08-07
author: Brian Kohles
tags: recipes code
categories: code
layout: post
---

This is the conclusion to my previous post at: [Converting Recipes from Paprika to Markdown]({% post_url 2020-06-30-Converting-recipes-paprika-to-markdown %})

I've been working on converting all of my text based data to markdown to make it more portable.

I've found a great iOS app to use for my recipes, using [Grocery App]({% post_url 2020-06-29-Grocery_-_Smart_Shopping_List %}), which I covered yesterday.

Now the problem is that I need to get all my recipes out of Paprika 3 and into markdown. This post will cover that process as it unfolds, and provide the code needed if you come across the same problem.

To automate this I wrote a couple of python scripts. One to convert the recipes from Paprika format to Markdown for blog posting. And a second to convert the blog recipes for use by the app, and to copy them to the proper icloud directory.

For me I'll be running the process to convert the recipes to my blog once, and then will be tweaking the recipe formats there, then will routinely run the script to update the app data. Any new recipes I create/find will be added directly to my blog, and will then auto import to the app.

The scripts and information for running them can be found on my Gitlab:

- [Gitlab - Paprika to Grocery Markdown](https://gitlab.com/briankohles/paprika-to-grocery-markdown)
