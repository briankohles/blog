---
title: Hike - Quartz Peak - Sierra Estrella Wilderness, Phoenix, AZ
date: 2021-04-15
author: Brian Kohles
tags: hike quartz_peak
categories: hike
layout: post
---

## The Basics

This is a hike I did by myself earlier in the year, I had the opportunity to do it again with some friends recently, and it was an amazing day. When I did the hike previously on the weekend (back in January) the parking lot (about 10 spaces) was nearly full when I arrived at 7am. This time we hiked on a Thursday beginning at 8am, and didn't come across anyone else on the trail in the 6 hours that we were there.

This trail is rated as hard and is about 2,500ft ascent over 2.8 miles, additionally the last 1/4 mile or so is a scramble that requires some route finding as the trail is not clearly marked the whole way. The trail will be in full sun with virtually no trees or shade within 2 hours of sun rise.

When you make it to the top you are rewarded with great views of the valley, and experiencing a peak, which is quite literally solid quartz. It is something that is much different than other hikes in the area.

## Map

<iframe src='https://www.gaiagps.com/datasummary/folder/4d150212-e912-4a1e-9401-7b15efc9469b/?layer=GaiaTopoRasterFeet&embed=True' style='border:none; overflow-y: hidden; background-color:white; min-width: 320px; width:100%; height: 420px;' scrolling='no' seamless='seamless'></iframe>

## Images & Video

This is a 360 image, you can pan around the image.

<iframe class="ku-embed" frameborder="0" allowfullscreen allow="xr-spatial-tracking; gyroscope; accelerometer" scrolling="no" src="https://kuula.co/share/7QvYp?fs=1&vr=0&zoom=10&thumbs=1&alpha=0.60&info=0&logo=0" style='border:none; overflow-y: hidden; background-color:white; min-width: 320px; width:100%; height: 420px;'></iframe>