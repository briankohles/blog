---
title: Unifi Notes
layout: page
collection: wiki
author: Brian Kohles
tags: wiki unifi
categories: unifi
---

# unifi notes
- password in vault

# update & upgrade
<!-- Enable a code snippet that is copyable -->
{% capture code %}sudo apt-get update && sudo apt-get upgrade -y{% endcapture %}
{% include code.html code=code lang="shell" %}