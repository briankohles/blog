---
title: dig
layout: page
collection: wiki
author: Brian Kohles
tags: wiki dig
categories: wiki
---

## query a specific DNS server to resolve a hostname
<!-- Enable a code snippet that is copyable -->
{% capture code %}dig -t A +noall +answer <host to query> @<DNS Server IP>{% endcapture %}
{% include code.html code=code lang="shell" %}
