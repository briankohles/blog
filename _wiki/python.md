---
title: Python
layout: page
collection: wiki
author: Brian Kohles
tags: wiki python
categories: wiki
---

## start a basic web server
<!-- Enable a code snippet that is copyable -->
{% capture code %}# cd into the directory containing the code
python -m SimpleHTTPServer 8000{% endcapture %}
{% include code.html code=code lang="python" %}

## Use argparse
<!-- Enable a code snippet that is copyable -->
{% capture code %}import argparse

parser = argparse.ArgumentParser(description='get arguments')
parser.add_argument('--platform', '-p',
                    nargs="*",
                    help='Platform to apply')
parser.add_argument('--ldif', '-l',
                    required=True,
                    help='LDIF to apply')
parser.add_argument('--env', '-e',
                    nargs="*",
                    help='List of environments to apply change to')
args = parser.parse_args()

print(args)

print(args.env){% endcapture %}
{% include code.html code=code lang="shell" %}

## Pretty Print
<!-- Enable a code snippet that is copyable -->
{% capture code %}import pprint

pp = pprint.PrettyPrinter(compact=False,indent=4)

pp.pprint(hosts){% endcapture %}
{% include code.html code=code lang="shell" %}
