---
title: LVM
layout: page
collection: wiki
author: Brian Kohles
tags: wiki lvm
categories: wiki
---

## lvextend - increase size of disk
<!-- Enable a code snippet that is copyable -->
{% capture code %}lvextend -L 40G /dev/vg01/lv_opendj{% endcapture %}
{% include code.html code=code lang="shell" %}
- increase disk to 40GB

<!-- Enable a code snippet that is copyable -->
{% capture code %}resize2fs /dev/vg01/lv_opendj{% endcapture %}
{% include code.html code=code lang="shell" %}
- increase filesystem to fill disk

<!-- Enable a code snippet that is copyable -->
{% capture code %}df -h{% endcapture %}
{% include code.html code=code lang="shell" %}
- to verify
