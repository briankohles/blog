---
title: splunk
layout: page
collection: wiki
author: Brian Kohles
tags: wiki splunk
categories: wiki
---

## Assign an extra field value
<!-- Enable a code snippet that is copyable -->
{% capture code %}eval hostType="old"{% endcapture %}
{% include code.html code=code lang="shell" %}

## Replace part of a string
<!-- Enable a code snippet that is copyable -->
{% capture code %}eval src=replace(CLIENT, ":.*", ""){% endcapture %}
{% include code.html code=code lang="shell" %}

## Compare fields
<!-- Enable a code snippet that is copyable -->
{% capture code %}where src != dest{% endcapture %}
{% include code.html code=code lang="shell" %}
