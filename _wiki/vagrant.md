---
title: vagrant
layout: page
collection: wiki
author: Brian Kohles
tags: wiki vagrant
categories: wiki
---

## Basics

Run the below from the directory where you want the VM stored

Init a new vagrant file using an existing build.

`vagrant init hashicorp/bionic64`

Start the machine.

`vagrant up`

SSH into the newly created server.

`vagrant ssh`

Destroy a machine

`vagrant destroy`