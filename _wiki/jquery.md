---
title: jquery
layout: page
collection: wiki
author: Brian Kohles
tags: wiki jquery
categories: wiki
---

## Selectors
<!-- Enable a code snippet that is copyable -->
{% capture code %}$("*"){% endcapture %}
{% include code.html code=code lang="shell" %}
- Selects all elements Try it 
<!-- Enable a code snippet that is copyable -->
{% capture code %}$(this){% endcapture %}
{% include code.html code=code lang="shell" %}
- Selects the current HTML element Try it 
<!-- Enable a code snippet that is copyable -->
{% capture code %}$("p.intro"){% endcapture %}
{% include code.html code=code lang="shell" %}
- Selects all <p> elements with class="intro" Try it 
<!-- Enable a code snippet that is copyable -->
{% capture code %}$("p:first"){% endcapture %}
{% include code.html code=code lang="shell" %}
- Selects the first <p> element Try it 
<!-- Enable a code snippet that is copyable -->
{% capture code %}$("ul li:first"){% endcapture %}
{% include code.html code=code lang="shell" %}
- Selects the first <li> element of the first <ul> Try it 
<!-- Enable a code snippet that is copyable -->
{% capture code %}$("ul li:first-child"){% endcapture %}
{% include code.html code=code lang="shell" %}
- Selects the first <li> element of every <ul> Try it 
<!-- Enable a code snippet that is copyable -->
{% capture code %}$("[href]"){% endcapture %}
{% include code.html code=code lang="shell" %}
- Selects all elements with an href attribute Try it 
<!-- Enable a code snippet that is copyable -->
{% capture code %}$("a[target='_blank']"){% endcapture %}
{% include code.html code=code lang="shell" %}
- Selects all `<a>` elements with a target attribute value equal to "_blank" Try it 
<!-- Enable a code snippet that is copyable -->
{% capture code %}$("a[target!='_blank']"){% endcapture %}
{% include code.html code=code lang="shell" %}
- Selects all `<a>` elements with a target attribute value NOT equal to "_blank" Try it 
<!-- Enable a code snippet that is copyable -->
{% capture code %}$(":button"){% endcapture %}
{% include code.html code=code lang="shell" %}
- Selects all `<button>` elements and `<input>` elements of type="button" Try it 
<!-- Enable a code snippet that is copyable -->
{% capture code %}$("tr:even"){% endcapture %}
{% include code.html code=code lang="shell" %}
- Selects all even `<tr>` elements Try it 
<!-- Enable a code snippet that is copyable -->
{% capture code %}$("tr:odd"){% endcapture %}
{% include code.html code=code lang="shell" %}
- Selects all odd `<tr>` elements Try it 
