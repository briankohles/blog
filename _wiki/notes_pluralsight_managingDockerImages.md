---
title: Notes - Pluralsight - Managing Docker Images
layout: page
collection: wiki
author: Brian Kohles
tags: wiki 
categories: wiki
---

- URL: https://app.pluralsight.com/library/courses/docker-images-managing/
- Working Directory: Documents/Courses/Pluralsight-ManagingDockerImages 

## M1: Course Overview

  - http://bootstrap-it.com/docker-images/

## M2: Introduction to Docker Images

  - `docker images` - list all local images
  - `docker build -t myimage .` - create image -t to tag image & . to use Dockerfile in current directory
  - `/var/lib/docker` - linux path to docker data
  - docker hub - repo of images
  - docker cloud - hosted browser based interface for managing docker related resources
  - Amazon EC2 Container Registry - Amazon AWS Registry

  - https://hub.docker.com
  - https://cloud.docker.com
  - Amazon ECS
  - Docker Trusted Registry
    - won't be discussed
    - Getting Started with Docker Datacenter on Pluralsight
  - Docker Registry
  - Docker Datacenter
    - hosted on own infrastructure
  
  - `docker run -d webserver` -d for detached mode

## M3: Managing Images Using Docker Hub

  - 

## M4: Installing a Private (and free) Docker Registry

## M5: Securing Your Images in Transit

## M6: Other Image Managing Tools
