---
title: bash
layout: page
collection: wiki
author: Brian Kohles
tags: wiki bash
categories: wiki
---

## Using Curly Braces

### create a backup of a file
<!-- Enable a code snippet that is copyable -->
{% capture code %}cp file.txt{,.bak}{% endcapture %}
{% include code.html code=code lang="shell" %}

### move a file in a long directory
<!-- Enable a code snippet that is copyable -->
{% capture code %}mv /a/really/really/long/directory/path/{old_file,new_file}{% endcapture %}
{% include code.html code=code lang="shell" %}


## print all environment variables
<!-- Enable a code snippet that is copyable -->
{% capture code %}set{% endcapture %}
{% include code.html code=code lang="shell" %}

## Set command prompt to display current GIT banch
### Add to .bash_profile
<!-- Enable a code snippet that is copyable -->
{% capture code %}# set git branch into the prompt
export PS1='[\[$(tput bold)\]\u@\h\[$(tput bold)\] \[$(tput setaf 2)\]`whichBranch` \[$(tput sgr0)\] \w] \n'{% endcapture %}
{% include code.html code=code lang="shell" %}

### create in ~/bin/whichBranch
<!-- Enable a code snippet that is copyable -->
{% capture code %} #!/bin/sh
G=`git branch 2>&1 | head -1 | sed -e 's/* //'`
NB=`echo $G | grep "Not a git repository" | wc -l`
if [ $NB -eq 1 ]; then
   echo "nab"
else
   #echo $G
   #if [`${#G}` -gt 15]; then
      echo "*"${G:${#G} - 14}
   #fi
fi{% endcapture %}
{% include code.html code=code lang="shell" file="~/bin/whichBranch" %}

## Use the history to run commands
<!-- Enable a code snippet that is copyable -->
{% capture code %}history{% endcapture %}
{% include code.html code=code lang="shell" %}
- list the recent commands

<!-- Enable a code snippet that is copyable -->
{% capture code %}history 5{% endcapture %}
{% include code.html code=code lang="shell" %}
- list the recent commands, but limit to the last 5
<!-- Enable a code snippet that is copyable -->
{% capture code %}!235{% endcapture %}
{% include code.html code=code lang="shell" %}
- (bang 235) execute command #235 from the history list

<!-- Enable a code snippet that is copyable -->
{% capture code %}!!{% endcapture %}
{% include code.html code=code lang="shell" %}
- (bang bang) execute the last command again. Handy to run a command again that you forgot to sudo for (sudo !!)

## for loop
<!-- Enable a code snippet that is copyable -->
{% capture code %}for VARIABLE in file1 file2 file3
do
	command1 on $VARIABLE
	command2
	commandN
done{% endcapture %}
{% include code.html code=code lang="shell" %}

## infinite loop
<!-- Enable a code snippet that is copyable -->
{% capture code %}while true; do echo 'Hit CTRL+C'; sleep 1; done{% endcapture %}
{% include code.html code=code lang="shell" %}

## recall arguments from previous commands
<!-- Enable a code snippet that is copyable -->
{% capture code %}!:2{% endcapture %}
{% include code.html code=code lang="shell" %}
- Recall second argument

<!-- Enable a code snippet that is copyable -->
{% capture code %}!:1-3{% endcapture %}
{% include code.html code=code lang="shell" %}
- Recall first 3 arguments
- Ex:
   - `echo a b c d e f`
   - `echo !:2-4` - Would run "echo b c d"
