---
title: rsync
layout: page
collection: wiki
author: Brian Kohles
tags: wiki rsync
categories: wiki
---

## Sync iCloud Drive folder to another location
<!-- Enable a code snippet that is copyable -->
{% capture code %}rsync -au /Volumes/Data_2TB/Users/briankohles/Library/Mobile\ Documents/com~apple~CloudDocs/ /Volumes/movies2/icloud_drive_rsync/{% endcapture %}
{% include code.html code=code lang="shell" %}
