---
title: MacOS Photos App
layout: page
collection: wiki
author: Brian Kohles
tags: wiki mac os photos
categories: wiki
---

## Fix “photos library is in use by another program” Error in OSX Photos

* Find your Photos Library.  Most likely in /Users/[you]/Pictures/Photos Library.
* Right-click/ctrl-click the package and select “Show Package Contents”
* Navigate to the “database” folder
* Delete the “photos.db.lock” and/or "photos.sqlite.lock" file
* Restart Photos
