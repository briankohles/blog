---
title: Notes - Pluralsight - Monitoring Key Systems with Prometheus Exporters
layout: page
collection: wiki
author: Brian Kohles
tags: wiki 
categories: wiki
---

- Working Directory: Documents/Courses/Pluralsight-MonitoringKeySystemswithPrometheusExporters
- Sample Code:

## Using Exporters

- Identify and install desired exporter on target system
- Create Prometheus job to pull data from exporter
- Prometheus will gather metrics and report on job status
- You can setup up multiple targets and muliple exporters



