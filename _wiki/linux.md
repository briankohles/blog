---
title: linux
layout: page
collection: wiki
author: Brian Kohles
tags: wiki Linux
categories: wiki
---

## SE Linux

Get the context of a process

<!-- Enable a code snippet that is copyable -->
{% capture code %}ps auxZ | grep -i httpd{% endcapture %}
{% include code.html code=code lang="shell" %}

Get the context of a file

<!-- Enable a code snippet that is copyable -->
{% capture code %}ls -laZ emmet.png terminator.png{% endcapture %}
{% include code.html code=code lang="shell" %}

Restore the context of a directory

<!-- Enable a code snippet that is copyable -->
{% capture code %}restorecon -Rv /srv/www{% endcapture %}
{% include code.html code=code lang="shell" %}

## systemctl

How to list all enabled services from systemctl

<!-- Enable a code snippet that is copyable -->
{% capture code %}systemctl list-unit-files | grep enabled{% endcapture %}
{% include code.html code=code lang="shell" %}
- will list all enabled services