# 3D Printing Filament Catalog

## Filament List

Moved to spreadsheet (currently Numbers)

## Filament Swatch Settings

Ender 3 V2 Neo

Slicer Set to 0.12mm & 100% infill

- **Extruder Temp:** 200º
- **Bed Temp:** 65º
- **Speed:** 75