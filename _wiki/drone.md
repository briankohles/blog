---
title: Drone Info
layout: page
collection: wiki
author: Brian Kohles
tags: wiki drone mavic mini dji
categories: wiki
---

## This page contains info & tips on drones

### Repairing corrupt Mavic Mini Video

- [DJI Fix Software](http://djifix.live555.com) (Mac, Win, or compile from source)

### Alternate Control Software

- [Litchi](http://www.flylitchi.com)

### Other Software

- https://www.hivemapper.com

###  Drone Shot Moves

[Jeven Dovey - YouTube](https://www.youtube.com/watch?v=I5G_s2soEKE)
- Shot #1 - Pull back & orbit
  - right stick
    - back & right
  - left stick
    - left
- Shot #2 - Dolly Shot
  - right stick
    - right & forward
- Shot #3 - Orbit
  - right stick
    - left
  - left stick
    - right
- Shot #4 - Orbit/Arc
  - right stick
    - left
  - left stick
    - slightly right
- Shot #5 - ascending
  - left stick
    - up
- Shot #6 - Dolly left -> right
  - right stick
    - right
- Shot #7 - Following orbit
  - right stick
    - up & right
  - left stick
    - left
- Editing #8 -
  - 1st - Reframe
  - 2nd - level frame
  - last - add post stabilization (final cut)
- Shot #9 - Top Down
  - add rotation while something moves through frame
- Shot #10 - Push forward & drop
  - right
    - forward
  - left
    - down
- Shot #11 - Push forward
  - right
    - forward
  - let subject enter the frame & go away, don’t follow
- Shot #12 - Tripod Shot
  - Use the camera as a floating tripod
- Shot #12 - Orbit with push
  - right
    - left & forward
  - left
    - right
- Shot #13 - Push forward while moving left/right
  - right
    - forward & left
  - subject in front as drone sweeps behind
- Tip #1
  - Always get multiple shots with each battery, don’t do the same shot over & over
  - Always shoot a top down

## 10 tips for cinematic shots

[Jeven Dovey - YouTube](https://www.youtube.com/watch?v=ilzORGeEHgI)
1. Use Manual white balance for video
2. normally use exposure lock
  - darken exposure slightly (-0.3, -0.7)
    - keep highlights @ about 80%
3. use ND filters
4. show scale
5. Unique perspectives
6. Sequencing - plan what you are going to shoot & how you will edit it together
  - close, medium, wide, different perspectives
7. contrast in the frame
8. Shoot early in the morning or late in the day
9. Color Grading in editing
  - bring mid tones down & bring shadows up a little
  - Then play with highlights
10. Post Stabilization & reframe
11. Research - build library of clips you like & why you like them


## Settings for Pro Video Footage

[ Drone Film Guide - YouTube](https://www.youtube.com/watch?v=8ufrD24BpDc)

- Turn on Histogram
- Set EV & set AE Lock

## Mavic Mini for Film Making

[Charlie Hunt](https://www.youtube.com/watch?v=4wv75LDbSWE)
- Histogram
- over exposure warning
- Guidelines
- Crosshair
- Exposure
  - Histogram & Overexposure warning
- Adjust EV
  - -0.7 & AE lock
- Allow upward gimbal rotation

## Tips for better landscape photography

[Stefan Malloch - YouTube](https://www.youtube.com/watch?v=oZQBOiQJ6RA)
- Always under expose
- Use HDR 

## Maneuvers

- Forward & Up & raise camera
  - press forward & up & slowly raise the gimbal from -90 up
- Crane Shot
  - camera forward as going up
- Follow Shot
  - Drone above & following subject below
- Fly through
  - flying past a subject/object
- 

## Other Settings

- Gimble Smoothness
- 5 - cinematic
- Gimble Speed
- 15 - cinematic
- Exposure lock
- Increase cache to 16GB
- Turn on cross & grid
- Manually select transmission frequency
- Make sure map orientation indicator is on
- Tap on map to turn on small preview map