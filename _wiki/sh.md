---
title: sh
layout: page
collection: wiki
author: Brian Kohles
tags: wiki sh
categories: wiki
---

## if regex
<!-- Enable a code snippet that is copyable -->
{% capture code %}if [[ $SERVER =~ ".test." ]]; then
        PASS=test;
else
        PASS=prod;
fi{% endcapture %}
{% include code.html code=code lang="shell" %}
