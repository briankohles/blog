---
title: GPG
layout: page
collection: wiki
author: Brian Kohles
tags: wiki gpg
categories: wiki
---

## Generate a new key
<!-- Enable a code snippet that is copyable -->
{% capture code %}gpg --gen-key generate a new key{% endcapture %}
{% include code.html code=code lang="shell" %}
- Will prompt for answers to questions
- Can the answers be fed as part of the command? Otherwise create a perl sub to do this.
- Each key in the keychain will have it's own password

<!-- Enable a code snippet that is copyable -->
{% capture code %}gpg --passphrase password --decrypt-file testfile.txt.gpg -o testfile2.txt{% endcapture %}
{% include code.html code=code lang="shell" %}
- Use a password to decrypt the file

<!-- Enable a code snippet that is copyable -->
{% capture code %}gpg --list-keys{% endcapture %}
{% include code.html code=code lang="shell" %}

```
-e, --encrypt [file]
-d, --decrypt [file]
--list-keys [names]
--list-public-keys [names]
-K, --list-secret-keys [names]
--delete-key name
--delete-secret-key name
--delete-secret-and-public-key name
--export [names]
--export-secret-keys [names]
--export-secret-subkeys [names]
--import [files]
-r, --recipient name
-a, --armor
-o, --output file
-q, --quiet
--batch
--homedir directory
--debug flags
--passphrase-fd n
	If you use  0  for  n, the passphrase will be read from stdin.
--passphrase string
```

## GPG2 started requiring the agent to run
## presumably you already have a .gnupg directory, but this won't hurt even if you do
<!-- Enable a code snippet that is copyable -->
{% capture code %}mkdir -p -m 700 ~/.gnupg{% endcapture %}
{% include code.html code=code lang="shell" %}

## now let's create the socket.  The "p" below says make it a "pipe" (aka: fifo or socket)
<!-- Enable a code snippet that is copyable -->
{% capture code %}mknod -m 700 ~/.gnupg/S.gpg-agent p{% endcapture %}
{% include code.html code=code lang="shell" %}

## And give gpg-agent a whirl:
<!-- Enable a code snippet that is copyable -->
{% capture code %}gpg-agent --daemon{% endcapture %}
{% include code.html code=code lang="shell" %}

From: [http://www.linuxquestions.org/questions/linux-security-4/gpg-gpg-agent-can%27t-connect-to-root-gnupg-s-gpg-agent-611843/](http://www.linuxquestions.org/questions/linux-security-4/gpg-gpg-agent-can%27t-connect-to-root-gnupg-s-gpg-agent-611843/) 

To prevent gpg from asking for the passphrase we echo the passphrase into the command. However we still had to add in the option "--batch" to get the "--passphrase-fd 0" option to work with the pipe
<!-- Enable a code snippet that is copyable -->
{% capture code %}echo "passphrase" | gpg --batch --passphrase-fd 0 --homedir /home/jobs/.gnupg --no-tty --output file.txt --skip-verify --decrypt encryptedfile.pgp{% endcapture %}
{% include code.html code=code lang="shell" %}

From: [https://www.centos.org/forums/viewtopic.php?t=44661](https://www.centos.org/forums/viewtopic.php?t=44661)
