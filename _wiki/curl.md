---
title: curl
layout: page
collection: wiki
author: Brian Kohles
tags: wiki curl
categories: wiki
---

### use curl to pull back LDAP data
<!-- Enable a code snippet that is copyable -->
{% capture code %}curl -u 'user:password' 'ldap://host:2389/o=baseDN?uid,cn?sub?(uid=UIDVALUE)'{% endcapture %}
{% include code.html code=code lang="shell" %}

## Using Cookies to create/manage a session
### Request the homepage to establish your session & cookies.
<!-- Enable a code snippet that is copyable -->
{% capture code %}curl -v -b cookiejar.txt -c cookiejar.txt -i -L <URL>{% endcapture %}
{% include code.html code=code lang="shell" %}
* Pay attention to the output of this, there may be a hidden form field named “_csrf”, grab the value of this field.

### post credentials to login
<!-- Enable a code snippet that is copyable -->
{% capture code %}curl -v -b cookiejar.txt -c cookiejar.txt -i -L -d "username=<userID>&password=<pasword>&_csrf=<string from above including dashes>" <URL>{% endcapture %}
{% include code.html code=code lang="shell" %}
* After login you are given the homepage & can continue to make requests
