---
title: Notes - Pluralsight - Getting Started with Hashicorp Vault
layout: page
collection: wiki
author: Brian Kohles
tags: wiki 
categories: wiki
---

- Course: https://app.pluralsight.com/course-player?clipId=e909bfe0-bb16-461a-b0ef-524264929ca0

- Working Directory: Documents/Courses/Pluralsight-GettingStartedWithHashicorpVault


## Dev server:
You may need to set the following environment variable:

    $ export VAULT_ADDR='http://127.0.0.1:8200'

The unseal key and root token are displayed below in case you want to
seal/unseal the Vault or re-authenticate.

Unseal Key: BBlrjQjbdmrHiTeF42kPdtlxsvWTOkSOc6Z2WjRNcdc=
Root Token: s.yzlBNRdiiiYNsjCflfAg6E9W

## Module 3 - Working with Secrets

