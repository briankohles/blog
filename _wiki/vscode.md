---
title: Visual Studio Code
layout: page
collection: wiki
author: Brian Kohles
tags: wiki vsCode
categories: wiki
---

* [Snippet Generator](https://snippet-generator.app/)
* Extensions I like:
  * [text](https://link)
  * [Ansible](https://github.com/VSChina/vscode-ansible)
  * [YAML](https://github.com/redhat-developer/vscode-yaml)
  * [vim](https://github.com/VSCodeVim/Vim)

* Themes
  * [Dracula](https://github.com/dracula/visual-studio-code)
  * [Apprentice Theme](https://github.com/amariampolskiy/apprentice-vscode)
  * [Material Icons](https://github.com/PKief/vscode-material-icon-theme)
  
  
  
  
  After a year, you now have tabs in vscode. To have a new tab, press Ctrl+Shift+P, type Preference: Open User Settings, look for workbench.editor.enablePreview, set the value to false.
That should fix it.



## Install extensions via command line:

```bash
code --list-extensions
code --install-extension ms-vscode.cpptools
code --uninstall-extension ms-vscode.csharp
```

## VS Code help for extension install

```bash
code -h

Extensions Management:
  --extensions-dir <dir>                                         Set the root path for extensions.
  --list-extensions                                              List the installed extensions.
  --show-versions                                                Show versions of installed extensions, when
                                                                 using --list-extension.
  --install-extension (<extension-id> | <extension-vsix-path>)   Installs an extension.
  --uninstall-extension (<extension-id> | <extension-vsix-path>) Uninstalls an extension.
  --enable-proposed-api <extension-id>                           Enables proposed API features for an extension.
```

And[`--force --install-extension`](https://github.com/Microsoft/vscode/issues/58434#issuecomment-421023851)to update