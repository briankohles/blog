# 3D Printing

## Getting Started

- [Tom's Hardward - How to make Your First 3d Print](https://www.tomshardware.com/how-to/3d-print-an-stl-file)
- [Ender-3 V2 Neo - Setup and First Print](https://youtu.be/vaDvZqpQ_t8)
- [Dremel Lesson Plans](https://3pitech.com/pages/dremel-lesson-plans)

## Ender 3 V2 Neo Setup

- Bed Leveling
  - Turning knobs:
    - Counterclockwise - platform away from nozzle (down)
      - **Left:** Loosen grip on paper
    - Clockwise - platform closer to nozzle (up)
      - **Right:** Tighten grip on paper
- **On Mac Place Scripts in:** `~/Library/Application\ Support/cura/5.2/scripts`
- [Adding Thumbnails To STL Files](https://www.crealityexperts.com/enabling-ender-3-v2-neo-model-preview-with-cura)

## Tuning

- [Stress Free First Layer Calibration in less than 5 Minutes to perfection](https://www.printables.com/model/251587-stress-free-first-layer-calibration-in-less-than-5)
- [Full Print Bed Test](https://www.printables.com/model/303825-full-print-bed-many-printers)
- [Temperature and Bridges Tower](https://www.printables.com/model/39810-improved-all-in-one-temperature-and-bridging-tower)



## 3D Model Sites

- [3D Warehouse](https://3dwarehouse.sketchup.com)
- [Fab365](https://fab365.net)
- [redpah](https://redpah.com)
- [Threeding](https://www.threeding.com)
- [zortrax Library](https://library.zortrax.com)
- [3D Ocean](https://3docean.net)
- [Thangs](https://thangs.com)
- [pinshape](https://pinshape.com)
- [Free3D](https://free3d.com)
- [YouMagine](https://www.youmagine.com)
- [NASA 3D Models](https://nasa3d.arc.nasa.gov/models/printable)
- [Smithsonian](https://3d.si.edu)
- [African Fossils](https://africanfossils.org)
- [Printables](https://www.printables.com)
- [3DfindIt.com](https://www.3dfindit.com/en/)
- [Shapeways](https://www.shapeways.com/marketplace)
- [NIH 3D Print Exchange](https://3dprint.nih.gov)
- [Yeggi](https://www.yeggi.com)
- [SketchFab](https://sketchfab.com)
- [STL finder](https://www.stlfinder.com)
- [My Mini Factory](https://www.myminifactory.com)
- [STL Flix](https://www.stlflix.com)
- [Cults 3D](https://cults3d.com)
- [3D Exports](https://3dexport.com)
- [CG Trader](https://www.cgtrader.com)
- [Turbo Squid](https://www.turbosquid.com)
- [GrabCAD](https://grabcad.com/library/category/3d-printing?page=3&time=all_time&sort=recent)


## 3D Design Software

## Ender 3 V2 Neo Setup

 1, Preheat bed & nozzle to PLA temps
 1. retract filament
 1. Tighten down bed levelers all the way compress, so the springs are as tight as possible. This will help keep the bed steady
 2. Run through autohome
 3. move
  1. Move Z to zero
4. disable stepper
5. move head around & level each corner using the adjustment knobs
  1. Go around at least 3 times.
  2. Check level using a piece of paper it should just feel a tiny amount of friction
6. Autolevel
7. Set Z-offset until paper has just a touch of friction between nozzle & bed


