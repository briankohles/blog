---
title: 'Notes - Pluralsight - Python: The Big Picture'
layout: page
collection: wiki
author: Brian Kohles
tags: wiki 
categories: wiki
---

- Working Directory: Documents/Courses/Pluralsight-PythonTheBigPicture
- Sample Code:


## Module 2: What is Python

### The Python Philosophy

- Beautiful is better than ugly
- Explicit is better than implicit
- Simple is better than complex
- Complex is better than complicated
- Readability counts

### What Makes Python Different?

- Extensible Design
- Community Involved Design

## Module 3: When and Where is Python Being Used?

- Linux Scripting and Administration
- Web Development
- Application Scripting
- Data Science

## Module 4: First Steps with Python

## Module 5: Continuing Your Python Journey