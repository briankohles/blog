---
title: nmap
layout: page
collection: wiki
author: Brian Kohles
tags: wiki nmap
categories: wiki
---

## Using nmap to check SSL/TLS version & Ciphers
<!-- Enable a code snippet that is copyable -->
{% capture code %}nmap -Pn -sV --script ssl-enum-ciphers -p 636 <FQDN>{% endcapture %}
{% include code.html code=code lang="shell" %}
