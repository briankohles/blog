---
title: cut
layout: page
collection: wiki
author: Brian Kohles
tags: wiki cut
categories: wiki
---

## cut characters 20-40 from the line
<!-- Enable a code snippet that is copyable -->
{% capture code %}cut -c 14-20{% endcapture %}
{% include code.html code=code lang="shell" %}

## cut long lines to fit in the terminal
<!-- Enable a code snippet that is copyable -->
{% capture code %}cut -c -$(($COLUMNS-5)) access_log*{% endcapture %}
{% include code.html code=code lang="shell" %}
* $COLUMNS is the current width of the teminal window
