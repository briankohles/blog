---
title: Off Roading Info
layout: page
collection: wiki
author: Brian Kohles
tags: wiki offroading
categories: wiki
---

# Off Road Recovery Companies

## Arizona

### Arizona 4x4 Off Road Recovery LLC

- **Location:** Mesa, AZ
- **Phone:** 602-697-8306
- **Email:** 
- **Web:** [https://arizona4x4offroadrecovery.com](https://arizona4x4offroadrecovery.com)

### Arizona Back Country Recovery

- **Location:** Phoenix, AZ
- **Phone:** 602-505-0538
- **Email:** backcountryrecovery@msn.com
- **Web:** [https://www.backcountryrecovery.info](https://www.backcountryrecovery.info)

### Arizona Off Road Rescue and Towing LLC

- **Location:** Phoenix, AZ
- **Phone:** 480-233-4192
- **Email:** eliot@arizonaoffroadrescue.com
- **Web:** [http://arizonaoffroadrescue.com/contact.html](http://arizonaoffroadrescue.com/contact.html)
- **Radio:** Marine radio - channel 16   (156.800Mhz)

### United Towing Flagstaff

- **Location:** Flagstaff, AZ
- **Phone:** 928-526-1839
- **Email:** info@unitedtowingflagstaff.com
- **Web:** https://unitedtowingflagstaff.com/off-road-recovery.php

## Utah

### Winder Towing Inc

- **Location:** Hurricane, UT
- **Phone:** 435-635-2231
- **Email:** mattstowingandrecovery51@gmail.com
- **Web:** [https://windertowing.com/](https://windertowing.com/)




### Company Template

- **Location:**
- **Phone:**
- **Email:**
- **Web:** []()
