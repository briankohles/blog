---
title: Notes - Pluralsight - Getting Started with Docker
layout: page
collection: wiki
author: Brian Kohles
tags: wiki 
categories: wiki
---

- URL: https://app.pluralsight.com/library/courses/docker-getting-started/discussion
- Working Directory: Documents/Courses/Pluralsight-GettingStartedWithDocker

## Module 3 - Installing Docker

Follow docker install from docker.com

## Module 4 - Working with Containers

`docker info` -shows details of the docker server

`docker ps` - list our running containers

`docker ps -a` - list all containers, even those that have recently exited

`docker images` - list all the currently downloaded images

### Containers and images

- images are stopped containers
- containers are running images

`docker pull ubuntu` - download latest ubuntu image & store locally

`docker pull ubuntu:14.04` - downlaod the 14.4 version

`docker rmi ubuntu:14.04` - remove an image

`docker start <container>` - start a container

`docker stop <container>` - stop a container

`docker rmi <container>` - delete a container

### Lifecycle of a container

Containers are great for non persistent workloads. They can handle workloads with persistence as well.

`docker run -d --name web -p 80:8080 nigelpoulton/pluralsight-docker-ci`

- run a container
- `-d` - detached/daemon mode
- `--name` - a unique name for the container
- `-p 80:8080` - map port 80 on the docker server to port 8080 in the container.
- list the image to run

`docker stop web` - stop our container

`docker start web` - start our container

`docker run -it --name temp ubuntu:latest /bin/bash`

- `-it` - interactive & terminal
- run /bin/bash

`Cntrl-P+Q` - while in a running container to exist without stopping the container. If it's something like a single process bash container.

`docker ps -aq` - list all docker processes & show only the dockerID, good to use with other commands to perform actions on sets of servers

`docker stop $(docker ps -aq)` - stop all containers

`docker rm $(docker ps -qa)` - rm all containers



## Module 5 - Swarm Mode and Microservices

- Swarm is a group of docker engines in a cluster
- Multiple managers for H/A
- worker nodes execute tasks
- Services
  - delarative way of running & scaling tasks
  - we declare how many we want & docker makes sure it keeps that number running
- Tasks
  - Assigned to workers
    - containers (could be other things in future)

### Building a Swarm

- 3 managers
- 3 workers

### Create a swarm

`docker swarm init` - start up a swarm

`docker swarm join-token manager` - get the token to join another manager

`docker swarm join-token worker` - get the token to join another worker

`docker info` - will give details on swarm config

A manager is also a worker node

`docker node ls` - can only be run from a manager. will list the nodes of a swarm

`docker node promote <ID>` - promote a worker to a manager

### Services

- simplifying large scale deployments
- declarative
  - desired state <-> actual state

`docker service create --name psight1 -p 8080:8080 --replicas 5 nigelpoulton/pluralsight-docker-ci` - deploy a new service using port 8080 in the container & exposed. Create 5 tasks/replicas

`docker service ls` - list out the services, high level

`docker service ps psight1` - show processes for the service, details of where the task is running

`docker service inspect psight1` - shows all the config of the service

The swarm members are load balanced by docker via the routing mesh.

### Desired State Scaling

`docker service scale psight1=7` - scale our service up to 7 tasks

`docker service update --replicas 10 psight` - same as `docker service scale psight=10`

Adding a new node/worker doesn't automatically scale existing tasks across the new node.

### Rolling Updates

`docker network create -d overlay ps-net` - create an overlay network

`docker network ls` - list networks

`docker service inspect -pretty psight2` - show the service info

### Stacks & Distributed Application Bundles (DAB)

- **Stack** - application comprising multiple services
  - deployed from distributed application bundles (DAB)

config for a stack is in docker-compose.yaml file.

`docker-compose bundle` - will build a docker-compose.yaml into a DAB

`docker stack tasks <appname>` - list all the tasks for a service

`docker stack rm <appname>` - delete the stack app

## Modeul 6 - What's Next!

- Related courses
  - Docker Deep Dive - Nigel Poulton
  - Docker with DevOps Automated Workflows - Nigel Poulton
  - Docker for Web Developers - Dan Wahlin
  - CD using Docker and Ansible - Justin Menga

- DockerCon