---
title: tr
layout: page
collection: wiki
author: Brian Kohles
tags: wiki tr
categories: wiki
---

## Remove EOL from a file
<!-- Enable a code snippet that is copyable -->
{% capture code %}tr -d '\r' $file{% endcapture %}
{% include code.html code=code lang="shell" %}

<!-- Enable a code snippet that is copyable -->
{% capture code %}tr -d '\n' < input.txt > output.txt{% endcapture %}
{% include code.html code=code lang="shell" %}


## Convert file contents to lower case
<!-- Enable a code snippet that is copyable -->
{% capture code %}tr A-Z a-z < file.txt{% endcapture %}
{% include code.html code=code lang="shell" %}

