---
title: Papercraft Info
layout: page
collection: wiki
author: Brian Kohles
tags: wiki papercraft
categories: wiki
---

## SVG Files

* [SimpleMaps - Interactive Maps & Data](https://simplemaps.com/us)

## Software

* [Unfolder or Mac](https://www.unfolder.app)
* [Template Maker](https://www.templatemaker.nl/en/)
* [Pepakura Designer](https://tamasoft.co.jp/pepakura-en/index.html)
* [Pepakura Viewer for Silhouette Cameo](https://tamasoft.co.jp/pepakura-en/download/viewer_cut/index.html)
* [Meshlab](https://www.meshlab.net)
* [blender 3D](https://www.blender.org)
* [PaperCraft Maker](https://www.papercraft-maker.com)
* [Convert Silhouette to SVG](http://ideas-r-us-software.uk/FileConverters/SilhouetteStudioConverter.aspx)
  * [tutorial](https://www.darinascrafts.com/convert-silhouette-studio3-to-svg/)
* 


## Youtube Channels

* [Mandee Thomas](https://www.youtube.com/channel/UCJfwwmxxu2BEkvInSDpC3Pg)

## Instruction

* [Low-Poly Paper Sculptures](https://www.instructables.com/Low-Poly-Paper-Sculptures/)
* 

## Projects & Stores

* [Lia Griffith](https://liagriffith.com)
  * https://liagriffith.com/felt-fox-stuffie/
  * https://liagriffith.com/felt-red-fox-stuffie/
  * https://liagriffith.com/felt-polar-bear-stuffie/
  * https://liagriffith.com/needle-felted-llamas/
  * https://liagriffith.com/felt-raccoon-stuffie/
  * https://liagriffith.com/felt-brown-bear-stuffie/
  * https://liagriffith.com/printable-acorn-treat-box/
  * https://liagriffith.com/felt-woodland-pocket-pals/
  * https://liagriffith.com/felt-bear-stuffie/
* [Dreaming Tree - Design Files](https://3dsvg.com)
* [Simply Crafty SVGs](https://www.simplycraftysvgs.com)

## Materials

* [Expressions Vinyl](https://expressionsvinyl.com)
* [Online Labels](https://www.onlinelabels.com/products/ol177?src=dlc-264)
* [12x12 Cardstock Shop](https://www.12x12cardstock.shop)

## Selling

* [So Fontsy](https://sofontsy.com/pages/open-shop)
* [Creative Market](https://creativemarket.com/sell)
* [The Hungry JPEG](https://thehungryjpeg.com/open-a-store)
* [DesignBUNDLES.net](https://designbundles.net/?ref=hXGMVp)
* [How to Make SVG Files to Sell](https://leapoffaithcrafting.com/how-to-make-svg-files-to-sell/)
* [Udemy - Etsy 101](https://www.udemy.com/course/etsy-101-how-to-create-and-sell-svg-cutting-files-on-etsy/)
* [Etsy](https://www.etsy.com)
* 
