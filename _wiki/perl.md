---
title: Perl
layout: page
collection: wiki
author: Brian Kohles
tags: wiki perl
categories: perl
---

## Convert Microsoft tick time to epoch

This does a simple conversion of Microsoft tick time into unix epoch.

Tick is the time that MS used to store many dates in Active Directory, such as last login and password last modified.

<!-- Enable a code snippet that is copyable -->
{% capture code %}## tickToEpoch
sub tickToEpoch {
        my ($args) = @_;
        # arguments are passed in as a hashref in the format of;
        # $args->{timeintick}
 
        my $winTick = 10000000;
        my $secFromTickToEpoch = 11644473600;
 
        return ($args->{timeintick}/$winTick - $secFromTickToEpoch);
}
## /tickToEpoch{% endcapture %}
{% include code.html code=code lang="perl" %}

Formula from: [Stack Overflow](https://stackoverflow.com/questions/6161776/convert-windows-filetime-to-second-in-unix-linux#6161842)

## Passing data to subs

### Defining the sub
<!-- Enable a code snippet that is copyable -->
{% capture code %}sub Test {
	my ($args) = @_;
	# aruments are passed in as a hashRef in the format of:
	# $args->{host} -- hostname to connect to
	# $args->{bindDn} -- bind DN (optional)
	
	return $item;
}{% endcapture %}
{% include code.html code=code lang="perl" %}

### Calling the sub
<!-- Enable a code snippet that is copyable -->
{% capture code %}my $return = test({
    host => 'testHost.test.com',
});{% endcapture %}
{% include code.html code=code lang="shell" %}

## Read data from perl code instead of from file
<!-- Enable a code snippet that is copyable -->
{% capture code %}while(<DATA>) {
        chomp();
        print "DATA LINE: $_;\n";
}

__DATA__
Line 1 item 1, Line 1 item 2
Line 2 item 1, Line 2 item 2
Line 3 item 1, Line 3 item 2
Line 4 item 1, Line 4 item 2{% endcapture %}
{% include code.html code=code lang="perl" %}

## create a hash from an array using map
<!-- Enable a code snippet that is copyable -->
{% capture code %}my %currentServers = map { $_ => 1 } @{$currentServers->{servers}};{% endcapture %}
{% include code.html code=code lang="perl" %}

# hashes
<!-- Enable a code snippet that is copyable -->
{% capture code %}my %hash = (
	key => 'value',
	key2 => 'value2',
);{% endcapture %}
{% include code.html code=code lang="perl" %}

* Keys don't need to be quoted when using a fat arrow
* keys will return a list of all keys
* values will return a list of all values

## replace text in a bunch of files one liner
<!-- Enable a code snippet that is copyable -->
{% capture code %}
perl -pi -e 's/about news\.$/about selling advertising space\./' file*
-p              loop and swallow the files, and print default.
-i              edit the files in-place
-e              do the command{% endcapture %}
{% include code.html code=code lang="shell" %}

## Create a spinner to show that something is running
<!-- Enable a code snippet that is copyable -->
{% capture code %}my @spinner = qw( | / - \ );

$|=1;
for my $major(0..15) {
        my $spin_cnt=0;
        for (1..4) {
                print $major." ".$spinner[$spin_cnt]."\r";
                $spin_cnt++;
                sleep 15;
        }
}
$|=0;{% endcapture %}
{% include code.html code=code lang="perl" %}

## create regex "objects"
<!-- Enable a code snippet that is copyable -->
{% capture code %}#!/usr/bin/perl -w

use strict;

my @find = (
        qr/cat/i,
        qr/dog/i
);

my @strings = ('cat','dog','bear','badger bear','dog cat');

foreach my $string (@strings) {
        print "STRING:$string;\n";
        foreach my $find (@find) {
                print "MATCH:$find;\n" if ($string =~ /$find/);
        }
}{% endcapture %}
{% include code.html code=code lang="perl" %}

## use a regex to separate a string into multiple strings (leaves the original string unaffected.
<!-- Enable a code snippet that is copyable -->
{% capture code %}my ($date, $time) = $dateTime =~ /(^\d{4}-\d{2}-\d{2})-(\d\d)/m;{% endcapture %}
{% include code.html code=code lang="perl" %}

## modify @INC at runtime
<!-- Enable a code snippet that is copyable -->
{% capture code %}Use "perl -I /path/to/library" to add a path to @INC @ runtime.{% endcapture %}
{% include code.html code=code lang="perl" %}

## creating Arrays of Arrays and referencing them
<!-- Enable a code snippet that is copyable -->
{% capture code %}my @AoA = ( [1,2,3],[4,5,6] ); # creates an Array of Arrays
	# the curved brackets make the grouping into a list directly.
print $AoA[1][0]; # retrieve the values directly.

my $ref_to_AoA = [ [1,2,3], [4,5.6] ]; # create a reference to an Array of Arrays
	# the square brackets make the grouping into a referenced list.
print $ref_to_AoA->[1][0]; # retrieve the values through dereferencing{% endcapture %}
{% include code.html code=code lang="perl" %}

## Indenting text while using heredoc
<!-- Enable a code snippet that is copyable -->
{% capture code %}# all in one
(my $VAR = <<HERE_TARGET) =~ s/^\s+//gm;
        your text
        goes here
HERE_TARGET{% endcapture %}
{% include code.html code=code lang="perl" %}
