---
title: 'Notes - Pluralsight - Python: The Big Picture'
layout: page
collection: wiki
author: Brian Kohles
tags: wiki 
categories: wiki
---

- Working Directory: Documents/Courses/Pluralsight-GettingStartedWithPrometheus
- Sample Code:

## Module 2: Understanding How Prometheus Works

- Prometheus Server
  - Scheduler fetches metrics from targets using a pull model
  - Time Series Database
  - Http API
  - Web UI
  - Alerting system

- What makes Prometheus so awesome
  - each target runs an exporter
  - exporter makes metrics available via http endpoint
  - Prometheus pulls data from the http endpoint

- Histograms are good for putting data into buckets, like sorting out good vs bad response times into buckets

- each label added creates a new timeseries of the data

## Module 3: Running and Configuring Prometheus



## Module 4: Querying Metrics with PromQL

### Selector Basics:

- `metric_name` - will list out all values for metric
- `metric_name{label="value"}` - use a selector, like a where clause in SQL
- label selectors
  - `label="value"` - string compare
  - `label=~"value.*"` - regex compare
- `metric_name[5m]` - To see a range of results instead of the most recent specify the range
  - all samples in 5 minute span

### Operators

- `sum(metric_name)` - total of all label values
- `sum without(job) (metric_name)` - like a group by in SQL with a having clause

### Functions

- `delta(metric_name[1h])` - difference between first & last values in a range
  - not an agregation
  - good for gauges
- `avg(delta(meric_name[1h]))`
- `rate(metric_name[5m])` - output is a gauge

### Demos

Demos are in the Courses content folder.