---
title: Git
layout: page
collection: wiki
author: Brian Kohles
tags: wiki git
categories: wiki
---

## set git to remember password in memory
<!-- Enable a code snippet that is copyable -->
{% capture code %}git config --global credential.helper cache{% endcapture %}
{% include code.html code=code lang="shell" %}


## Git Reference (quick refrence with links to specific doc & book sections for more details.)
http://gitref.org/

## Initialize a git repo. This can be used to do minor management without a server
<!-- Enable a code snippet that is copyable -->
{% capture code %}cd /project/directory/
git init
git add somefile.ext
git commit -m 'Initial commit.'{% endcapture %}
{% include code.html code=code lang="shell" %}

## Restore a deleted file
* CD to the directory that contained the file
* run git log to see list of all commits
<!-- Enable a code snippet that is copyable -->
{% capture code %}git log --stat{% endcapture %}
{% include code.html code=code lang="shell" %}

* Output:
``` output
commit 1f51a84aa9b7e...f2438e0a2340d40c
Author: Brian Kohles <brian.kohles@email.com>
Date:   Wed Jan 20 16:35:08 2016 +0000

		deleting old data conversion scripts
	
	.../files/opt/opendj/dataConversion/b2e/convertTdsToOpenDjB2E.sh                  | 19 -------------------
	.../files/opt/opendj/dataConversion/b2e/removeDupeMembers.pl                      | 38 --------------------------------------
	2 files changed, 57 deletions(-)
```
* find the commit that caused the delete
``` output
543445a8d06...7d7b7e8a8446 (Wed Jan 20 20:45:02 2016 +0000)
1f51a84aa9b7e...80a2340d40c (Wed Jan 20 16:35:08 2016 +0000)
```
* 1f51a84aa9b7e - deleted in this commit
* 54aaadd6d09 - checkout from the previous commit
* view the changes for that commit

## Compare a branch to origin/prod listing only the changed files
### if already in the branch
<!-- Enable a code snippet that is copyable -->
{% capture code %}git diff --name-status origin/prod{% endcapture %}
{% include code.html code=code lang="shell" %}

<!-- Enable a code snippet that is copyable -->
{% capture code %}git diff --name-only origin/prod...{% endcapture %}
{% include code.html code=code lang="shell" %}

### checking a branch that you don't have checked out
<!-- Enable a code snippet that is copyable -->
{% capture code %}git diff --name-status origin/prod...origin/dev_new{% endcapture %}
{% include code.html code=code lang="shell" %}

### compare staged changes against HEAD?
<!-- Enable a code snippet that is copyable -->
{% capture code %}git diff --staged{% endcapture %}
{% include code.html code=code lang="shell" %}

### show the number of changes per file
<!-- Enable a code snippet that is copyable -->
{% capture code %}git diff --stat origin/prod origin/dev_new{% endcapture %}
{% include code.html code=code lang="shell" %}

## Comparing an individual file against prod
<!-- Enable a code snippet that is copyable -->
{% capture code %}git diff origin/prod infra_modules/ldap_opendj_b2e_server_v1_0_0/data/categories/appl/common.yaml{% endcapture %}
{% include code.html code=code lang="shell" %}

## branch - list branches
<!-- Enable a code snippet that is copyable -->
{% capture code %}git branch{% endcapture %}
{% include code.html code=code lang="shell" %}
- list the checked out branches, current branch is green with a *

<!-- Enable a code snippet that is copyable -->
{% capture code %}git branch -a{% endcapture %}
{% include code.html code=code lang="shell" %}
- list all branches local and remote

<!-- Enable a code snippet that is copyable -->
{% capture code %}git branch -r{% endcapture %}
{% include code.html code=code lang="shell" %}
- list all remote branches

## Remove a file from a commit after adding it
<!-- Enable a code snippet that is copyable -->
{% capture code %}git reset <file>{% endcapture %}
{% include code.html code=code lang="shell" %}


## switch to a different branch
<!-- Enable a code snippet that is copyable -->
{% capture code %}git checkout <branch>{% endcapture %}
{% include code.html code=code lang="shell" %}


## merge changes from origin/prod into the current branch
<!-- Enable a code snippet that is copyable -->
{% capture code %}git merge origin/prod{% endcapture %}
{% include code.html code=code lang="shell" %}


## create a new copy of the puppet repo
<!-- Enable a code snippet that is copyable -->
{% capture code %}git clone git@host:puppet-sbx/puppet-environments.git{% endcapture %}
{% include code.html code=code lang="shell" %}


## Keep the code up to date with everything else happening
<!-- Enable a code snippet that is copyable -->
{% capture code %}git fetch # grab the current updates for this branch{% endcapture %}
{% include code.html code=code lang="shell" %}
{% capture code %}git merge origin/prod # merge updates from origin{% endcapture %}
{% include code.html code=code lang="shell" %}
{% capture code %}git push # push all the changes back into our branch{% endcapture %}
{% include code.html code=code lang="shell" %}

## General GIT flow
### Initial setup
- Create a new copy of the repo
{% capture code %}git clone git@host:puppet-sbx/puppet-environments.git{% endcapture %}
{% include code.html code=code lang="shell" %}
- Checkout the branch you want to work with
{% capture code %}git checkout <branch>{% endcapture %}
{% include code.html code=code lang="shell" %}
- Get the current version of the branch
{% capture code %}git fetch{% endcapture %}
{% include code.html code=code lang="shell" %}

### Ongoing work
- Make changes to the files as needed
- Add changes to commit
<!-- Enable a code snippet that is copyable -->
{% capture code %}git add [list of files/paths]{% endcapture %}
{% include code.html code=code lang="shell" %}
- Commit added changes (only commits locally)
<!-- Enable a code snippet that is copyable -->
{% capture code %}git commit -m "[commit comment]"{% endcapture %}
{% include code.html code=code lang="shell" %}
- Push commits to remote
<!-- Enable a code snippet that is copyable -->
{% capture code %}git push{% endcapture %}
{% include code.html code=code lang="shell" %}

### On a regular basis (daily/weekly) - Pull in changes that have occurred outside of current branch
- get updates from branch
<!-- Enable a code snippet that is copyable -->
{% capture code %}git fetch --prune # --prune to remove files.{% endcapture %}
{% include code.html code=code lang="shell" %}
- merge updates from origin (merge will also do a commit of the updates)
<!-- Enable a code snippet that is copyable -->
{% capture code %}git rebase origin/prod{% endcapture %}
{% include code.html code=code lang="shell" %}
- push these updates back up to our branch
<!-- Enable a code snippet that is copyable -->
{% capture code %}git push{% endcapture %}
{% include code.html code=code lang="shell" %}

# checkout file to how it was at the previous commit (does this actually go back to the last commit, or the state of HEAD?)
<!-- Enable a code snippet that is copyable -->
{% capture code %}git checkout -- <file(s)>{% endcapture %}
{% include code.html code=code lang="shell" %}

# Setup git pull to use rebase by default. Should already be in /etc/gitconfig
<!-- Enable a code snippet that is copyable -->
{% capture code %}git config --global branch.autosetuprebase always{% endcapture %}
{% include code.html code=code lang="shell" %}

# Setup git push to use simple push by default. Should already be in /etc/gitconfig
<!-- Enable a code snippet that is copyable -->
{% capture code %}git config --global push.default simple{% endcapture %}
{% include code.html code=code lang="shell" %}

# Speed up git by enabling preloadindex. Should already be in /etc/gitconfig
<!-- Enable a code snippet that is copyable -->
{% capture code %}git config --global core.preloadindex true{% endcapture %}
{% include code.html code=code lang="shell" %}

# graph out the log data into branches
<!-- Enable a code snippet that is copyable -->
{% capture code %}git log --graph --decorate --pretty=oneline --abbrev-commit prod origin/prod temp{% endcapture %}
{% include code.html code=code lang="shell" %}

## Figure out how to undo something in GIT
[https://sethrobertson.github.io/GitFixUm/fixup.html](https://sethrobertson.github.io/GitFixUm/fixup.html)

## Show files ready to push
<!-- Enable a code snippet that is copyable -->
{% capture code %}git diff --numstat [remote repo/branch]{% endcapture %}
{% include code.html code=code lang="shell" %}

## Determine what branches are merged to master
<!-- Enable a code snippet that is copyable -->
{% capture code %}git branch --merged master{% endcapture %}
{% include code.html code=code lang="shell" %}
- lists branches merged into master

<!-- Enable a code snippet that is copyable -->
{% capture code %}git branch --merged{% endcapture %}
{% include code.html code=code lang="shell" %}
- lists branches merged into HEAD (i.e. tip of current branch)

<!-- Enable a code snippet that is copyable -->
{% capture code %}git branch --no-merged{% endcapture %}
{% include code.html code=code lang="shell" %}
- lists branches that have not been merged 

## Show the difference between a commit and its ancestor
<!-- Enable a code snippet that is copyable -->
{% capture code %}git diff COMMIT_HASH^ COMMIT_HASH{% endcapture %}
{% include code.html code=code lang="shell" %}
- The ^ gets the first parent of the commit then compares that to the actual commit
- On linux this can be shorted to `git diff COMMIT{^,}`
	- the "{^,}" will expand & create two arguments passed to git diff one with the carat & the second without

## pick specific commits to pull from one branch into another. Useful for creating hot fixes to push out before a full branch is ready.
<!-- Enable a code snippet that is copyable -->
{% capture code %}git cherry-pick COMMIT_HASH{% endcapture %}
{% include code.html code=code lang="shell" %}
* add "--stdin" to paste in a list of hashes


## using vimdiff to solve git merge conflicts
[http://www.rosipov.com/blog/use-vimdiff-as-git-mergetool/](http://www.rosipov.com/blog/use-vimdiff-as-git-mergetool/)

## Creating a diff from one branch to move changes to a new branch
- While in the branch with the changes:
<!-- Enable a code snippet that is copyable -->
{% capture code %}git diff origin/prod > changes.diff{% endcapture %}
{% include code.html code=code lang="shell" %}
- review the diff files & delete any sections (the whole section) for changes you don't want to apply.
- checkout the branch you want to move the changes to:
- move the changes.diff file to the root of your git directory
- apply the changes

<!-- Enable a code snippet that is copyable -->
{% capture code %}git apply -p1 changes.diff{% endcapture %}
{% include code.html code=code lang="shell" %}
- if this fails with "patch does not apply" try:

<!-- Enable a code snippet that is copyable -->
{% capture code %}git apply --reject --whitespace=fix mychanges.patch{% endcapture %}
{% include code.html code=code lang="shell" %}
- This will apply each patch individually & create a rejects file (in the same location as the file to be patched) for any patches that fail. This narrows down the manual changes to just a couple.

## Find what files have changed since your branch diverged from origin
<!-- Enable a code snippet that is copyable -->
{% capture code %}git diff --name-only origin...{% endcapture %}
{% include code.html code=code lang="shell" %}

## Using Stash
<!-- Enable a code snippet that is copyable -->
{% capture code %}git stash{% endcapture %}
{% include code.html code=code lang="shell" %}
- stash the current changes

<!-- Enable a code snippet that is copyable -->
{% capture code %}git stash list{% endcapture %}
{% include code.html code=code lang="shell" %}
- list out the current stashes
- stash@{#} -- this is the stash number

<!-- Enable a code snippet that is copyable -->
{% capture code %}git stash show -p <stash@{#}>{% endcapture %}
{% include code.html code=code lang="shell" %}
- Show the diff of the last or given stash

<!-- Enable a code snippet that is copyable -->
{% capture code %}git stash apply <stash@{#}>{% endcapture %}
{% include code.html code=code lang="shell" %}
- Apply the last or given stash

<!-- Enable a code snippet that is copyable -->
{% capture code %}git stash drop <stash@{#}>{% endcapture %}
{% include code.html code=code lang="shell" %}
- remove the named stash from the stack

<!-- Enable a code snippet that is copyable -->
{% capture code %}git stash pop{% endcapture %}
{% include code.html code=code lang="shell" %}
- does an apply & a drop of the stash
