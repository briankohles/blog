---
title: sar
layout: page
collection: wiki
author: Brian Kohles
tags: wiki sar
categories: wiki
---

## Viewing statistics

Type the following command to get today’s CPU load average statistics:

<!-- Enable a code snippet that is copyable -->
{% capture code %}sar -q | less{% endcapture %}
{% include code.html code=code lang="shell" %}

More info:

https://abdussamad.com/archives/167-CPU-load-average-history-in-Linux.html
