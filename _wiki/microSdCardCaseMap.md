---
title: Micro SD Card Case
layout: page
collection: wiki
author: Brian Kohles
tags: wiki microSdCardCase
categories: wiki
---

## Micro SD Card Case

- 1 - **Raspberry Pi Image**
  - user: pi
  - password: raspberry
  - docker cluster setup
  - unifi controller
- 2 - **NDS games**
- 3 - **Raspberry Pi image**
  - cluster? P4 image
- 4 - **Unknown**
  - Not Raspberry Pi
  - Not Mac readable
- 5 - **Raspberry Pi image**
  - Docker
  - user: pi
  - password: raspberry
  - 32gb sandisk ultra
- 6 - Raspberry Pi Image
- 7 - Raspberry Pi Image
- 8 - 
- 9 - 
- a - RetroPi
- b - 
- c - Raspberry Pi Image
- d - blank 16Gb
- e - blank 32Gb
- f - blank 64Gb
- g - blank 64Gb

