---
title: Sed
layout: page
collection: wiki
author: Brian Kohles
tags: wiki sed
categories: wiki
---

## replace string
<!-- Enable a code snippet that is copyable -->
{% capture code %}sed -e 's/\/.*\///g'{% endcapture %}
{% include code.html code=code lang="shell" %}
- remove /.*/ (directory structure from file)

## join ldif lines together
<!-- Enable a code snippet that is copyable -->
{% capture code %}sed -e :a -e '$!N;s/\n //;ta' -e 'P;D' file{% endcapture %}
{% include code.html code=code lang="shell" %}
- Append lines that start with a space to the previous line

## insert a line after a specific line number
<!-- Enable a code snippet that is copyable -->
{% capture code %}sed -i "3i hello" t.txt{% endcapture %}
{% include code.html code=code lang="shell" %}
- Insert a line at the 3rd line of the file t.txt that says "hello"
- -i is to replace the existing file rather than creating new.
<!-- Enable a code snippet that is copyable -->
{% capture code %}sed -i "6i exit 0;" dirinit.sh {% endcapture %}
{% include code.html code=code lang="shell" %}
- insert "exit 0;" after the 6th line of the file

## delete a specific line
<!-- Enable a code snippet that is copyable -->
{% capture code %}sed -e '/uniquemember: cn=null/Id' test.ldif{% endcapture %}
{% include code.html code=code lang="shell" %}
- delete lines  that match string
- I = case insensitive

## convert to lowercase
<!-- Enable a code snippet that is copyable -->
{% capture code %}sed -e 's/\(.*\)/\L\1/'{% endcapture %}
{% include code.html code=code lang="shell" %}

