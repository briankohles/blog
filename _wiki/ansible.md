---
title: ansible
layout: page
collection: wiki
author: Brian Kohles
tags: wiki ansible
categories: wiki
---

## Quickstart

### Install yum
<!-- Enable a code snippet that is copyable -->
{% capture code %}sudo yum install ansible{% endcapture %}
{% include code.html code=code lang="shell" %}

### Mac
{% capture code %}pip install ansible{% endcapture %}
{% include code.html code=code lang="shell" %}

## Documentation
{% capture code %}ansible-doc copy{% endcapture %}
{% include code.html code=code lang="shell" %}

## Commands

### Run a command against a list of severs
{% capture code %}ansible *.test.example.com -i server.list -m shell -a "hostname" -u USER_TO_RUN_AS -kK{% endcapture %}
{% include code.html code=code lang="shell" %}

- server.list is a file containing a list of server FQDNs

### Copy a file to the target
{% capture code %}ansible -m copy -a "src=master.gitconfig dest=~/.gitconfig" localhost{% endcapture %}
{% include code.html code=code lang="shell" %}

### Ansible Config File Locations

- Ansible will process the below list and use the first file found, all others are ignored.
  - ANSIBLE_CONFIG (an environment variable)
  - ansible.cfg (in the current directory)
  - .ansible.cfg (in the home directory)
  - /etc/ansible/ansible.cfg


