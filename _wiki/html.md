---
title: HTML
layout: page
collection: wiki
author: Brian Kohles
tags: wiki html
categories: wiki
---

## Create a simple bar graph in CSS
<p><p data-height="500" data-theme-id="4812" data-slug-hash="abdybMJ" data-default-tab="result" class="codepen">See the Pen <a href="https://codepen.io/briankohles/details/abdybMJ">Simple CSS Bar Chart</a> by Brian Kohles (<a href="http://codepen.io/briankohles">@briankohles</a>) on <a href="http://codepen.io">CodePen</a>.</p>
<script async src="//codepen.io/assets/embed/ei.js"></script></p>

## Using Div instead of tables
### *NOTE:* NEVER DO THIS!!!!
This is BAD. HTML tables are made for Tabular data. Using Divs instead of tables makes the table not able to copy/paste into something like excel.

Tables are good for tabular data, not good for layout.
