---
title: Notes - Pluralsight - Getting Started with Ansible
layout: page
collection: wiki
author: Brian Kohles
tags: wiki 
categories: wiki
---

- Working Directory: Documents/Courses/Pluralsight-GettingStartedWithAnsible
- Sample Code: https://github.com/g0t4/course-ansible-getting-started

- [ ] Install homebrew
  - [ ] Install bat
  - [ ] Install tree
    - tree -C alias
  - [ ] jq
  - [ ] httpie
  - [ ] curl
  - [ ] wget

## Ad-hoc
### Commands run:
<!-- Enable a code snippet that is copyable -->
{% capture code %}ansible -m copy -a "src=master.gitconfig dest=~/.gitconfig" localhost{% endcapture %}
{% include code.html code=code lang="shell" %}

<!-- Enable a code snippet that is copyable -->
{% capture code %}ansible -m copy -a "src=master.gitconfig dest=~/.gitconfig" --check --diff localhost{% endcapture %}
{% include code.html code=code lang="shell" %}

<!-- Enable a code snippet that is copyable -->
{% capture code %}ansible -m shell -a "hostname" -vvvv  localhost{% endcapture %}
{% include code.html code=code lang="shell" %}



## Managing Mac with Ansible
- install homebrew
- download linux setup from gitlab
<!-- Enable a code snippet that is copyable -->
{% capture code %}ansible -m copy -a "src=master.gitconfig dest=~/.gitconfig" --check --diff localhost{% endcapture %}
{% include code.html code=code lang="shell" %}
- Manage these types of file with git repo

<!-- Enable a code snippet that is copyable -->
{% capture code %}ansible -m homebrew -a "name=bat state=latest" localhost{% endcapture %}
{% include code.html code=code lang="shell" %}
- install bat command from homebrew

<!-- Enable a code snippet that is copyable -->
{% capture code %}ansible -m homebrew -a "name=jq state=latest" localhost{% endcapture %}
{% include code.html code=code lang="shell" %}

## Playbooks
[Intro to Playbooks](https://docs.ansible.com/ansible/latest/user_guide/playbooks_intro.html)

<!-- Enable a code snippet that is copyable -->
{% capture code %}ansible-playbook playbook.yaml{% endcapture %}
{% include code.html code=code lang="shell" %}

## spin up a quick docker container
<!-- Enable a code snippet that is copyable -->
{% capture code %}docker run --rm -it python bash{% endcapture %}
{% include code.html code=code lang="shell" %}

## connect to multiple machines using ansible
<!-- Enable a code snippet that is copyable -->
{% capture code %}ansible-console containers{% endcapture %}
{% include code.html code=code lang="shell" %}
- containers is defined in the inventory file as 3 docker containers

## Module 4

### Basic Auth

### Vault Policies

- Govern: Who, What, How for access
- Policy is in HCL or JSON
- Variables available for identities in path logic for dynamic policies
- can specify parameters
- two out of box policies: default & root policies

Policy Document:

path "path_of_secret_data/[*]" {
  capabilities = ["create","read","update"...]
  required parameters = ["param_name"]
  allowed parameters = {
    param_name = ["list","of","values"]
  }
  denied_paramters = {
    param_name = ["list","of","values"]
  }
}

Working with policies

## list all policies

`vault policy list`

## create a policy

`vault policy write [policy] [policy_file.hcl]`

## update a policy

`vault write sys/policy/[policy] policy=[policy_file.hcl]`

## delete a policy

`vault delete sys/policy[policy]`

## Client Tokens

- Policies are added to tokens
- updates to policies have an immediate affect on existing tokens
- creating new policies does not update existing tokens
- tokens are the foundation of access within vault
  - even if you auth via LDAP or something else it just gives you a token

# Module 5 - How to setup Configure & Operate Vault Server as an Admin

## Vault Server Architecture

### Storage

- over 19 options currently supported
- consul is kinda the default

## Installation Process for prod

- Installation Scenario
  - Virtual Network
    - Vault Subnet
      - Vault VM
        - HTTPS on 8200 default port
        - Public IP in front of vault server
        - talks on 8201 for clustering
        - Consul Agent for storage
          - Gossip & RPC protocols
          - Consul agent runs server on 8500
        - vault configured in HA with another vault server
    - Consul Subnet
      - Consul VM
        - Replication on port 8300
    - Communication between the two

## storage and configuration

## Server Operations

## Conclusion

- nothing leaves the barrier unencrypted
- everything in the barrier is audited
- plan your pilot to support HA

# Module 6 - Audting Actions in Vault

- everything is audited
- auditing must be available
- sensitive data is hashed by default