---
title: Test Code Snippet
layout: page
collection: wiki
author: Brian Kohles
tags: wiki testCodeSnippet
categories: wiki
---

<button type="button" class="button-reset w2 h2 br-100 bg-primary5 white hover-bg-primary6 absolute bottom--075 right--075 flex flex-column items-center justify-center"><span data-balloon="Copy Code" data-balloon-pos="left"><svg aria-hidden="true" focusable="false" data-prefix="fas" data-icon="clipboard" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 384 512" class="svg-inline--fa fa-clipboard fa-w-12"><path fill="currentColor" d="M384 112v352c0 26.51-21.49 48-48 48H48c-26.51 0-48-21.49-48-48V112c0-26.51 21.49-48 48-48h80c0-35.29 28.71-64 64-64s64 28.71 64 64h80c26.51 0 48 21.49 48 48zM192 40c-13.255 0-24 10.745-24 24s10.745 24 24 24 24-10.745 24-24-10.745-24-24-24m96 114v-20a6 6 0 0 0-6-6H102a6 6 0 0 0-6 6v20a6 6 0 0 0 6 6h180a6 6 0 0 0 6-6z" class=""></path></svg></span></button>


Posting Jekyll code
{% capture code %}{% raw %}{% capture code %}test file with name{% endcapture %}{% include code.html file="myFileName" code=code lang="javascript" %}{% endraw %}{% endcapture %}
{% include code.html code=code lang="javascript" %}


{% capture code %}test1{% endcapture %}
{% include code.html code=code lang="javascript" %}

{% capture code %}test2{% endcapture %}
{% include code.html code=code lang="javascript" %}



<!-- Enable a code snippet that is copyable -->
{% capture code %}{% endcapture %}
{% include code.html code=code lang="javascript" %}






<!-- Enable a code snippet that is copyable -->
{% capture code %}{% endcapture %}
{% include code.html code=code lang="javascript" %}

{% highlight javascript linenos %} var foo = function(x) { return(x + 5); } foo(3) {% endhighlight %}









This is a demo post to show you how to write blog posts with markdown.  I strongly encourage you to [take 5 minutes to learn how to write in markdown](https://markdowntutorial.com/) - it'll teach you how to transform regular text into bold/italics/headings/tables/etc.

**Here is some bold text**

## Here is a secondary heading

Here's a useless table:

| Number | Next number | Previous number |
| :------ |:--- | :--- |
| Five | Six | Four |
| Ten | Eleven | Nine |
| Seven | Eight | Six |
| Two | Three | One |


How about a yummy crepe?

![Crepe](https://s3-media3.fl.yelpcdn.com/bphoto/cQ1Yoa75m2yUFFbY2xwuqw/348s.jpg)

It can also be centered!

![Crepe](https://s3-media3.fl.yelpcdn.com/bphoto/cQ1Yoa75m2yUFFbY2xwuqw/348s.jpg){: .mx-auto.d-block :}

Here's a code chunk:

~~~
var foo = function(x) {
  return(x + 5);
}
foo(3)
~~~

And here is the same code with syntax highlighting:

```javascript
var foo = function(x) {
  return(x + 5);
}
foo(3)
```

And here is the same code yet again but with line numbers:

{% highlight javascript linenos %}
var foo = function(x) {
  return(x + 5);
}
foo(3)
{% endhighlight %}

## Boxes
You can add notification, warning and error boxes like this:

### Notification

{: .box-note}
**Note:** This is a notification box.

### Warning

{: .box-warning}
**Warning:** This is a warning box.

### Error

{: .box-error}
**Error:** This is an error box.