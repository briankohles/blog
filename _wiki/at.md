---
title: at
layout: page
collection: wiki
author: Brian Kohles
tags: wiki at
categories: wiki
---

at is useful for scheduling jobs to run at a later time, and not attached to a terminal.

## schedule a script to run at a given time
<!-- Enable a code snippet that is copyable -->
{% capture code %}at 12:32 -f /usr/local/bin/backup-script{% endcapture %}
{% include code.html code=code lang="shell" %}

## list the current jobs
<!-- Enable a code snippet that is copyable -->
{% capture code %}at -l{% endcapture %}
{% include code.html code=code lang="shell" %}

- will display a job # next to each job

## cancel a job (replace # with the number from above.)
<!-- Enable a code snippet that is copyable -->
{% capture code %}atrm <#>{% endcapture %}
{% include code.html code=code lang="shell" %}
