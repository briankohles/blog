---
title: 'Notes - Pluralsight - Introduction To Application Development For Red Hat OpenShift Container Platform'
layout: page
collection: wiki
author: Brian Kohles
tags: wiki 
categories: wiki
---

- Working Directory: Documents/Courses/Pluralsight-IntroductionToApplicationDevelopmentForRedHatOpenShiftContainerPlatform

[https://app.pluralsight.com/paths/skills/introduction-to-application-development-for-red-hat-openshift-container-platform](https://app.pluralsight.com/paths/skills/introduction-to-application-development-for-red-hat-openshift-container-platform)

## Course #1: Deploying Applications to Red Hat OpenShift Container Platform

Applications used in the course

- Visual Studio Code
- Git
- NodeJS and other runtimes

Accounts used in this course

- Github
- OpenShift Online Account (or CodeReady Containers)
