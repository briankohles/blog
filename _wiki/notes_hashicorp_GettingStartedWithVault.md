---
title: Notes - hashicorp - Getting Started with Hashicorp Vault
layout: page
collection: wiki
author: Brian Kohles
tags: wiki 
categories: wiki
---

[https://learn.hashicorp.com/vault](https://learn.hashicorp.com/vault)

- Working Directory: Documents/Courses/hashicorp-GettingStartedWithVault

## install
<!-- Enable a code snippet that is copyable -->
{% capture code %}brew install vault{% endcapture %}
{% include code.html code=code lang="shell" %}

## Start dev server
<!-- Enable a code snippet that is copyable -->
{% capture code %}vault server -dev{% endcapture %}
{% include code.html code=code lang="shell" %}

## Set environment to connect to dev server
Copy "export VAULT_ADDR*" output from the start dev server command

<!-- Enable a code snippet that is copyable -->
{% capture code %}export VAULT_ADDR='http://127.0.0.1:8200'{% endcapture %}
{% include code.html code=code lang="shell" %}

run this in a new terminal


## Save the unseal key & root token from dev server output
<!-- Enable a code snippet that is copyable -->
{% capture code %}Unseal Key: lKlZILjPpCB0BhUJJkgVmvbruFpl0R4udmWwPAKiUGY=
Root Token: s.LVEafEYecTGGAkPDKMaoPgU8{% endcapture %}
{% include code.html code=code lang="shell" %}

Set the root token to environment variable
<!-- Enable a code snippet that is copyable -->
{% capture code %}export VAULT_DEV_ROOT_TOKEN_ID=s.LVEafEYecTGGAkPDKMaoPgU8{% endcapture %}
{% include code.html code=code lang="shell" %}


## Create secret
<!-- Enable a code snippet that is copyable -->
{% capture code %}vault kv put secret/hello foo=world excited=yes{% endcapture %}
{% include code.html code=code lang="shell" %}

## Get secret
<!-- Enable a code snippet that is copyable -->
{% capture code %}vault kv get secret/hello{% endcapture %}
{% include code.html code=code lang="shell" %}

## List secrets
<!-- Enable a code snippet that is copyable -->
{% capture code %}vault kv list secret/{% endcapture %}
{% include code.html code=code lang="shell" %}

## Delete Secret
<!-- Enable a code snippet that is copyable -->
{% capture code %}vault kv delete secret/hello{% endcapture %}
{% include code.html code=code lang="shell" %}

## Enable a Secrets Engine
<!-- Enable a code snippet that is copyable -->
{% capture code %}vault secrets enable -path=kv kv{% endcapture %}
{% include code.html code=code lang="shell" %}

## List Secrets Engines
<!-- Enable a code snippet that is copyable -->
{% capture code %}vault secrets list{% endcapture %}
{% include code.html code=code lang="shell" %}

## disable Secrets Engine
<!-- Enable a code snippet that is copyable -->
{% capture code %}vault secrets disable kv/{% endcapture %}
{% include code.html code=code lang="shell" %}


## Keys
<!-- Enable a code snippet that is copyable -->
{% capture code %}Unseal Key 1: TrRFd8iFAm41pC4h+bDuv8T9HNNYlQjCOE8Xb9nqvebb
Unseal Key 2: AB12sI3TevvrwZCAQC3uddqRpz725fmFtWmkpZL36Gb5
Unseal Key 3: rxg//u5mplAvxwX4bBdOTrqH3LPrGIbCpm5Ca+ZsJerz
Unseal Key 4: l7I/gutoa95rzuka+2drKt7Ma/d31Jpv64fdZiOFlGLH
Unseal Key 5: 4ce0QcntgWDSpotWN86K2RXzVMUTXxOU+6RQtJczWrMa

Initial Root Token: s.zd43bRuSZf94QBlifUPHjsUW{% endcapture %}
{% include code.html code=code lang="shell" %}

## Keys for consul
<!-- Enable a code snippet that is copyable -->
{% capture code %}Unseal Key 1: bmDWZs0YMs2uFI1mCbL4I6xZ2CGbhqujEYFa7hOJ2dSi
Unseal Key 2: O2apGQuZQLDloC74PVT1JnfXa2PWLJwLAVazX1lYHQRr
Unseal Key 3: nHFFT1SGX2wIykoZNmI4n13gSeOCRs4wQd7kYXR+Guiy
Unseal Key 4: urytX5ngzcCTraO/8jkPU4XmwMhZLkX/TFfW+W15fNqS
Unseal Key 5: j+4tdSMhKWaHxGYQPY3qX/0B0AYD5R2GWbunODQNT7gS

Initial Root Token: s.WKeiVpLVVlRGs9NQ35D2J6pB{% endcapture %}
{% include code.html code=code lang="shell" %}

