---
title: zsh
author: Brian Kohles
tags: wiki zsh
categories: wiki zsh
collection: wiki
layout: page
---

## enable zmv for complex move
<!-- Enable a code snippet that is copyable -->
{% capture code %}autoload zmv{% endcapture %}
{% include code.html code=code lang="shell" %}