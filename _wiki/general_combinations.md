---
title: general and combinations
layout: page
collection: wiki
author: Brian Kohles
tags: wiki general combinations
categories: wiki
---

## Determine ACSII code for a character
<!-- Enable a code snippet that is copyable -->
{% capture code %}printf "%d\n" "'-" #(standard dash){% endcapture %}
{% include code.html code=code lang="shell" %}

{% capture code %}printf "%d\n" "'–" #(em dash){% endcapture %}
{% include code.html code=code lang="shell" %}

## return random 3000 lines from a text file
<!-- Enable a code snippet that is copyable -->
{% capture code %}awk 'BEGIN {srand()} {print rand() " " $0}' file8000.txt
    | sort
    | tail -3000
    | sed 's/[^ ]* //'
    >file3000.txt{% endcapture %}
{% include code.html code=code lang="shell" %}
* From <http://stackoverflow.com/questions/7514896/select-random-3000-lines-from-a-file-with-awk-codes> 
