---
title: Unifi Network Equipment & Controller
layout: page
collection: wiki
author: Brian Kohles
tags: wiki 
categories: wiki
---

## When installing the Unifi Controller on Mac

- It seems to like to install with incorrect permissions. After install run:
<!-- Enable a code snippet that is copyable -->
{% capture code %}sudo chown -R briankohles:staff /Applications/UniFi*{% endcapture %}
{% include code.html code=code lang="shell" %}
- Without this chage there is an error about database permissions
